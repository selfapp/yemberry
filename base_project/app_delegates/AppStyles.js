import {StyleSheet} from 'react-native';

const appStyles = StyleSheet.create({

    layoutQuestionCard: {
        width: 345,
        height: 125,
        borderColor: '#fff',
        borderRadius: 4,
        marginTop: 12,
        backgroundColor: '#FFF',
        padding: 10,
        elevation: 0.5,
        color: '#454749',
    },
    styleGradientBtn: {
        height: 52.5,
        borderRadius: 4,
        marginTop: 12,
        marginLeft: 12,
        marginRight: 12,
        padding: 16,
    },
    styleVerticalLine: {
        borderLeftColor: '#E2E6EE',
        borderLeftWidth: 1,
        marginLeft: 8,
        marginRight: 8,
    },
    styleViewTxtInputBox: {
        height: 52.5,
        borderWidth: 1,
        borderColor: '#fff',
        borderRadius: 4,
        marginTop: 12,
        marginLeft: 12,
        marginRight: 12,
        backgroundColor: '#FFF',
        padding: 4,
        flexDirection: 'row',
        alignItems: 'center'
        //fontFamily: 'Cabin-Medium',
    },
    blackOverlayView: {
        backgroundColor: 'rgba(0,0,0,.9)',
        flex: 1,
        justifyContent: 'space-between',
    },
    styleViewGradientBtn: {
        flexDirection: 'row', height: 52.5,
        marginTop: 12,
        marginLeft: 12,
        marginRight: 12,
        borderRadius: 4,
    },
    styleViewBtnBlue: {
        width: 180,
        height: 45,
        borderColor: '#fff',
        borderRadius: 4,
        margin: 12,
        padding: 8,
        backgroundColor: '#0D92CA',
        alignItems: 'center'
    },
    styleGradientBtnTxt: {
        fontSize: 20,
        textAlign: 'center',
        color: '#FFF',
        fontWeight: 'bold',
        //fontFamily: 'Cabin-Medium',
    },
    styleViewBaseQuestionCard: {
        flex: 1,
        minHeight: 110,
        borderColor: 'white',
        borderRadius: 4,
        marginTop: 12,
        marginLeft: 12,
        marginRight: 12,
        backgroundColor: 'white',
        padding: 10,
        elevation: 0.5,
        flexDirection: 'column',
    },

    styleAvoidKeyboard: {flex: 1, justifyContent: 'center', paddingBottom: 20},
    styleTxtInputTxt: {fontSize: 20, color: '#454749', flex: 1},
    styleBaseViewProfilePic: {flex: 1, height: 242},
    styleViewFlexRowEnd: {flexDirection: 'row', alignItems: 'flex-end'},
    styleViewFlexColumnEnd: {flexDirection: 'column', alignItems: 'flex-end'},
    styleViewEditProfileIcon: {width: 30, height: 30, margin: 16, alignItems: 'center', justifyContent: 'center'},
    styleEditProfileIcon: {
        position: "absolute",
        bottom: 0,
        right: 0,
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    styleAlignItemsCenter: {alignItems: 'center'},
    styleBaseBackgroundView: {backgroundColor: '#f6f6f6'},

});

export default appStyles;