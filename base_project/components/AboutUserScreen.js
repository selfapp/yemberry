import React, {Component} from 'react';
import {Image, ScrollView, StyleSheet, Text, TextInput, TouchableWithoutFeedback, View} from 'react-native';
import * as firebase from 'firebase';
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from "react-native-linear-gradient";
import {keyNodeAllUsers, printLog} from "./BaseClass";
import appStyles from "../app_delegates/AppStyles";
import HomeTab from "./HomeTab";

class AboutUserScreen extends Component {

    static navigatorStyle = {
        drawUnderNavBar: false,
        navBarBackgroundColor: '#FFF',
        topBarElevationShadowEnabled: true,
        statusBarHidden: false,
        navBarComponentAlignment: 'center',
    };

    constructor(props) {
        super(props);
        this.state = {
            spinner: false,
            ques_ans: [],
            currentUserItem: '',
            likedByMe: false,
            likedByOther: false,
            txtLiked: 'LIKE',
            isThumbVisible: true,
            currUserUid: '',
        };
    }

    onNavigatorEvent(event) {
        if (event.type === 'NavBarButtonPress') {
            if (event.id === 'cancel') {
                this.props.navigator.pop({
                    screen: 'AboutUserScreen',
                });
            }
        }
    }

    _pushScreenIntoStack(screenName) {
        this.props.navigator.resetTo({
            screen: screenName,
        });
    }

    _showSpinner() {
        this.setState({spinner: true});
    }

    _dismissSpinner() {
        this.setState({spinner: false});
    }

    componentDidMount() {
        this.props.navigator.setTitle({
            title: "Dashboard"
        });
        this.props.navigator.setStyle({
            navBarBackgroundColor: '#FFF',
            navBarComponentAlignment: 'center',
        });
        let currUserUid = HomeTab._getCurrentUserNodeKey();
        let userItem = this.props.selectedUserItem;
        let _key = userItem._key;
        this.setState({currUserUid: currUserUid, currentUserItem: userItem});
        this._checkUserMatchState(currUserUid, _key, true);
    }

    async _checkUserMatchState(currUserKey, selectedUserKey, type) {
        let _this = this;
        let newNode = HomeTab._getUsersChatNodeKey(currUserKey, selectedUserKey);
        await firebase.database().ref('users/' + currUserKey + '/' + newNode).on('value', function (snapshot) {
            let data = snapshot.val();
            console.log('AboutUserScreen: checkUserMatchState fetched snapshot is', data);
            if (data !== null) {
                let likedByMe = data._me;
                let likedByOther = data._other;
                console.log('AboutUserScreen: checkUserMatchState fetched snapshot like state ' + likedByMe + ' ' + likedByOther);
                _this.setState({likedByMe: likedByMe, likedByOther: likedByOther});
                if (likedByMe) {
                    printLog('AboutUserScreen: checkUserMatchState fetched selectedUserKey ' + selectedUserKey);
                    _this.setState({txtLiked: 'LIKED', isThumbVisible: false});
                } else {
                    printLog('AboutUserScreen: checkUserMatchState liked by me is false.');
                }
            }
        });
        if (type) {
            this._fetchUserQuesAns(selectedUserKey);
        }
    }

    _fetchUserQuesAns(user_node) {
        let _this = this;
        firebase.database().ref(keyNodeAllUsers + user_node).on('value', function (snapshot) {
            console.log('#AboutUserScreen: fetchQuesAns snapshot is', snapshot.val());
            let s = snapshot.val();
            if (s != null) {
                _this.setState({ques_ans: snapshot.val().answers});
            } else {
                printLog('#AboutUserScreen: fetchQuesAns snapshot is null');
            }
        });
    }

    _onLikeBtnClick() {
        let _this = this;
        let currUserKey = _this.state.currUserUid;
        let selectedUserKey = _this.state.currentUserItem._key;
        printLog('#AboutUserScreen: at done btn click=> ' + currUserKey + ' ' + selectedUserKey);
        let userChatNode = HomeTab._getUsersChatNodeKey(currUserKey, selectedUserKey);
        let likedByOtherStatus = _this.state.likedByOther;
        printLog('#AboutUserScreen: fetchedNewNode=> ' + userChatNode + ' otherStatus= ' + likedByOtherStatus);
        firebase.database().ref(keyNodeAllUsers + currUserKey + '/' + userChatNode)
            .set({
                _me: true,
                _other: likedByOtherStatus,
            }).then(() => {
            firebase.database().ref(keyNodeAllUsers + selectedUserKey + '/' + userChatNode)
                .set({
                    _me: likedByOtherStatus,
                    _other: true,
                }).then(() => {
                printLog('#AboutUserScreen: at done end: both nodes created.');
                _this.setState({txtLiked: 'LIKED', isThumbVisible: false});
            });
        });
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#f6f5f7'}}>
                <ScrollView
                    keyboardShouldPersistTaps={'handled'}
                    style={styles.scrollViewContainer}
                    showsVerticalScrollIndicator={false}>
                    <View style={{margin: 12}}>
                        <Text style={{fontSize: 24, color: '#5C36AA', fontWeight: 'bold'}}>
                            {this.state.currentUserItem._name}, {this.state.currentUserItem._age}
                        </Text>
                    </View>
                    <View style={{flex: 1, flexDirection: 'row', marginLeft: 12,}}>
                        <View style={{flex: 0.2, flexDirection: 'row', marginTop: 6}}>
                            <View style={{marginRight: 6, alignItems: 'center'}}>
                                <Image
                                    source={require('../assets/gender/gender-icon.png')}
                                />
                            </View>
                            <View>
                                <Text style={styles.styleTxtOtherInfo}>
                                    {this.state.currentUserItem._gender}
                                </Text>
                            </View>
                        </View>
                        <View style={{flex: 0.2, flexDirection: 'row', marginTop: 6}}>
                            <View style={{marginRight: 6, alignItems: 'center'}}>
                                <Image style={{marginTop: 2}}
                                       source={require('../assets/location/location-icon.png')}
                                />
                            </View>
                            <View style={{width: 120}}>
                                <Text numberOfLines={1} style={styles.styleTxtOtherInfo}>
                                    {this.state.currentUserItem._location}
                                </Text>
                            </View>
                        </View>

                    </View>

                    <View style={{flex: 1}}>
                        {this._fetchQuestions()}
                    </View>

                    <View style={appStyles.styleAlignItemsCenter}>
                        <TouchableWithoutFeedback onPress={() => this._onLikeBtnClick()}>
                            <LinearGradient
                                start={{x: 0.0, y: 0.0}} end={{x: 1.0, y: 1.0}}
                                locations={[0, 0.3, 1.0]}
                                colors={['#8B5BE6', '#8B5BE6', '#54D2E1']}
                                style={styles.styleGradientBtnLike}>
                                <View style={{
                                    borderRadius: 4,
                                    backgroundColor: '#fff',
                                    flex: 1,
                                    flexDirection: 'row',
                                    padding: 8
                                }}>
                                    <View style={{flex: 0.35, alignItems: 'flex-start', justifyContent: 'center'}}>
                                        {this._renderLikeThumbView()}
                                    </View>
                                    <View style={{
                                        alignItems: 'flex-start',
                                        justifyContent: 'flex-start',
                                        flex: 0.65,
                                    }}>
                                        <Text style={styles.signInBtnText}>
                                            {this.state.txtLiked}
                                        </Text>
                                    </View>
                                </View>
                            </LinearGradient>
                        </TouchableWithoutFeedback>
                    </View>
                    <Spinner
                        color={'#54D2E1'}
                        visible={this.state.spinner}
                        cancelable={false}/>
                </ScrollView>
            </View>
        );
    }

    _renderLikeThumbView() {
        if (this.state.isThumbVisible) {
            return (
                <Image style={{marginBottom: 2}}
                       source={require('../assets/thumb-like/thumb.png')}
                />
            )
        } else {
            return null;
        }
    }

    _fetchQuestions() {
        return (
            this.state.ques_ans.map((item, index) => {
                return (
                    this._renderBaseCardView(item, index)
                );
            })
        )
    }

    _renderBaseCardView(item, index) {
        return (
            <View key={index} style={appStyles.styleViewBaseQuestionCard}>
                {this._renderQuestionView(item)}
                {this._renderAnswerView(item, index)}
            </View>

        );
    }

    _renderQuestionView(item) {
        return (
            <View style={styles.styleViewQuestionTxt}>
                <Text style={styles.styleQuestionTxt}>
                    {item._question}
                </Text>
            </View>
        );
    }

    _renderAnswerView(item, index) {
        return (
            <View style={[styles.styleViewQuestionTxt]}>
                <TextInput
                    key={item._id}
                    style={[styles.styleAnswerTxt]}
                    underlineColorAndroid="transparent"
                    multiline={true}
                    editable={false}
                    value={item._answer}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scrollViewContainer: {
        marginBottom: 2,
        flex: 1,
    },
    styleViewQuestionTxt: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginLeft: 4,
        marginRight: 4,
        marginTop: 4
    },
    styleQuestionTxt: {flex: 1, fontSize: 18, color: '#303030', fontWeight: 'bold'},
    styleAnswerTxt: {flex: 1, fontSize: 18, color: '#303030'},
    signInBtnText: {
        fontSize: 22,
        textAlign: 'center',
        color: '#505050',
        fontWeight: 'bold',
        marginBottom: 4,
        // fontFamily: 'Cabin-Regular',
    },
    styleGradientBtnLike: {
        width: 175,
        height: 45,
        borderRadius: 4,
        margin: 12,
        padding: 2,
        alignItems: 'center',
    },
});

export default AboutUserScreen;