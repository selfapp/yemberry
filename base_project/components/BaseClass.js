import Toast from 'react-native-simple-toast';

export const keyIsUserLoggedIn = 'isUserLoggedIn';
export const keyUserUID = 'userEmailId';
export const keyNodeChatRooms = 'chats/';
export const keyNodeProfile = '/profile';
export const keyNodeAllUsers = 'users/';
export const keySingleChatRoomStatus = '/_room_status';
export const keyGlobalQuestions = 'global_questions/';
export const keyNodeAnswers = '/answers';

export function printLog(log_msg) {
    console.log(log_msg);
}

export function showToastGlobal(msg) {
    Toast.show(msg, Toast.CENTER, Toast.SHORT);
}