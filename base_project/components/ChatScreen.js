import React, {Component} from 'react';
import {
    Dimensions,
    Image,
    Keyboard,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import * as firebase from 'firebase';
import ImagePicker from 'react-native-image-picker';
import {keyNodeChatRooms, keySingleChatRoomStatus, printLog} from "./BaseClass";
import HomeTab from "./HomeTab";
import * as BackHandler from "react-native/Libraries/Utilities/BackHandler.android";

var {height, width} = Dimensions.get('window');
var options = {
    title: 'Select Avatar',
    storageOptions: {
        skipBackup: true,
        path: 'you-for-you/images'
    }
};


export default class ChatScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            condition: false,
            message: "",
            messagesArr: [],
            image: null,
            currChatRoomKey: '',
            msgPic: '',
            _currChatRoomStatus: '',
            _user_1_UnreadCount: 0,
            _user_2_UnreadCount: 0,
        };

        this.contentHeight = null;
        this.scrollHeight = null;
        this.scrollY = null;
        ['handleKeyboardShow', 'handleKeyboardHide', 'handleLayout', 'handleContentChange', 'handleScroll',]
            .forEach((method) => {
                this[method] = this[method].bind(this)
            });

        this.props.navigator.setButtons({
            leftButtons: [{
                id: 'chat_screen_back',
                icon: require('../assets/back/back.png'),
                buttonColor: '#000',
            },
            ],
            animated: true,
        });
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));


    }


    static navigatorStyle = {
        tabBarHidden: true,
        drawUnderNavBar: false,
        navBarBackgroundColor: '#FFF',
        topBarElevationShadowEnabled: true,
        statusBarHidden: false,
        navBarComponentAlignment: 'center',
        statusBarColor: '#000000',

    };

    onNavigatorEvent(event) {
        if (event.type === 'NavBarButtonPress') {
            if (event.id === 'chat_screen_back') {
                printLog('backPressed at chat screen');
                let singleChatRoomNode = this.state.currChatRoomKey;
                this._updateUserStatusToOffline(singleChatRoomNode);
                this.props.navigator.pop({
                    screen: 'ChatScreen',
                });
            }
        }
    }

    componentWillMount() {
        Keyboard.addListener('keyboardDidShow', this.handleKeyboardShow);
        Keyboard.addListener('keyboardDidHide', this.handleKeyboardHide)
    }

    componentWillUnmount() {
        Keyboard.removeListener('keyboardDidShow', this.handleKeyboardShow);
        Keyboard.removeListener('keyboardDidHide', this.handleKeyboardHide)
    }


    componentDidMount() {
        let currChatNode = this.props._propsChatNode;
        let chatWithUserName = this.props.propsChatUserName;
        console.log('# At chat screen', currChatNode, chatWithUserName);
        this._onMessageDeliverSuccess(currChatNode);
        this.props.navigator.setTitle({
            title: chatWithUserName,
        });
        let _this = this;
        BackHandler.addEventListener('hardwareBackPress', function () {
            printLog('hardware-backPressed at chat screen');
            _this._updateUserStatusToOffline(currChatNode);
            _this.props.navigator.pop({
                screen: 'ChatScreen',
            });
            return true;
        });
    }

    handleKeyboardShow() {
        this.scrollToBottom()
    }

    handleKeyboardHide() {
        const {scrollY, scrollHeight, contentHeight} = this;

        // fix top blank if exists
        if (scrollY > contentHeight - scrollHeight) {
            this.refs.scroller.scrollTo({y: 0})
        }
        // fix bottom blank if exsits
        // else {
        //   this.scrollToBottom()
        // }
        else {
            this.refs.scroller.scrollTo({y: scrollY})
        }
    }

    handleScroll(e) {
        this.scrollY = e.nativeEvent.contentOffset.y
    }

    handleLayout(e) {
        this.scrollHeight = e.nativeEvent.layout.height
    }

    handleContentChange(w, h) {
        // repeated called on Android
        // should do diff
        if (h === this.contentHeight) return;
        this.contentHeight = h + height * 0.4;

        if (this.scrollHeight == null) {
            setTimeout(() => {
                this.scrollToBottomIfNecessary()
            }, 500)
        }
        else {
            this.scrollToBottomIfNecessary()
        }
    }

    scrollToBottomIfNecessary() {
        // todo: range detection
        this.scrollToBottom()
    }

    scrollToBottom() {
        const {scrollHeight, contentHeight} = this;
        if (scrollHeight == null) {
            return
        }
        if (contentHeight > scrollHeight) {
            this.refs.scroller.scrollTo({y: contentHeight - scrollHeight})
        } else {
            this.refs.scroller.scrollTo({y: 10000})
        }
    }

    handleTextChange(currentField, text, val) {
        this.setState({[val]: text});
        var next_text_field_index = (parseInt(currentField) - 1 + (text.length * 2));
        next_text_field_index = next_text_field_index > 4 ? 4 : next_text_field_index;
        next_text_field_index = next_text_field_index < 1 ? 1 : next_text_field_index;

        this.refs[next_text_field_index].focus();
    }

    _onMessageDeliverSuccess(nodeKey) {
        this.setState({currChatRoomKey: nodeKey});
        let _this = this;
        let chatRoomsRef = firebase.database().ref(keyNodeChatRooms);
        chatRoomsRef.child(nodeKey).once('value', function (snapshot) {
            if (snapshot.exists()) {
                let chatRoomData = snapshot.val();
                if (chatRoomData !== null) {
                    let arrMsgs = [];
                    snapshot.forEach(function (childSnapshot) {
                        let chatRoomChildData = childSnapshot.val();
                        console.log('#ChatScreen: onMessageDeliverSuccess: chatRoomChildData data', chatRoomChildData);
                        arrMsgs.push(chatRoomChildData);
                        console.log('#ChatScreen: onMessageDeliverSuccess: child added in array');
                    });
                    console.log('#ChatScreen: onMessageDeliver: now set state and show msgs in list.');
                    _this.setState({messagesArr: arrMsgs, message: ''});
                    //_this._showMessagesInListView();
                }
            }
        });
    }

    sendMessage(txtMsg, msgPicData) {
        let _this = this;
        this.setState({message: ''});
        let singleChatRoomNode = this.state.currChatRoomKey;
        let chatRoomsRef = firebase.database().ref(keyNodeChatRooms);
        let currUserKey = HomeTab._getCurrentUserNodeKey();
        chatRoomsRef.child(singleChatRoomNode).once('value', function (snapshot) {
            if (snapshot.exists()) {
                printLog('# ChatScreen: sendMessage ' + singleChatRoomNode + ' exists.');
                let singleChatRoomData = snapshot.val();
                let roomStatus = singleChatRoomData._room_status;
                let user_1_count = singleChatRoomData._unreadByUser_1;
                let user_2_count = singleChatRoomData._unreadByUser_2;
                if (roomStatus) {
                    _this.setState({
                        _currChatRoomStatus: roomStatus,
                        _user_1_UnreadCount: user_1_count,
                        _user_2_UnreadCount: user_2_count,
                    });
                }
            } else {
                printLog('ChatScreen: node ' + singleChatRoomNode + ' does not exists.');
            }
        }).then(() => {

            let roomStatus = _this.state._currChatRoomStatus;
            console.log('#ChatScreen: then()=> ', roomStatus);
            if (roomStatus) {
                let user1Status = roomStatus._isUser_1_online;
                let user2Status = roomStatus._isUser_2_online;

                let user1Key = roomStatus._user_1_id;
                let user2Key = roomStatus._user_2_id;

                let user_1_count = _this.state._user_1_UnreadCount;
                let user_2_count = _this.state._user_2_UnreadCount;
                printLog('# ChatScreen: sendMessage now pushing msg_child to chat node ' + user1Status + ' ' + user2Status + ' ' + user_1_count + ' ' + user_2_count);
                chatRoomsRef.child(singleChatRoomNode).push({
                    _msgSender: currUserKey,
                    _msgTxt: txtMsg,
                    _msgSendTime: new Date().getTime(),
                    _msgType: msgPicData !== null ? 'image' : 'text',
                    _msgImageData: msgPicData,
                    _readByUser_1: user1Status,
                    _readByUser_2: user2Status,
                }).then(() => {
                    console.log('#ChatScreen: msg pushed. now setting the last msg..............');
                    chatRoomsRef.child(singleChatRoomNode + keySingleChatRoomStatus + '/_lastMsg').set({
                        _msgSender: currUserKey,
                        _msgTxt: txtMsg,
                        _msgSendTime: new Date().getTime(),
                        _msgType: msgPicData !== null ? 'image' : 'text',
                        _msgImageData: msgPicData,
                        _readByUser_1: user1Status,
                        _readByUser_2: user2Status,
                    }).then(() => {
                        console.log('#ChatScreen: last msg set success..................', user_1_count, user_2_count);
                        if (!user1Status) {
                            let count = user_1_count + 1;
                            console.log('#ChatScreen: ===========', user1Status, user_1_count, count);
                            chatRoomsRef.child(singleChatRoomNode + '/_unreadByUser_1').set(count);
                        }
                        if (!user2Status) {
                            let count = user_2_count + 1;
                            console.log('#ChatScreen: ===========', user2Status, user_2_count, count);
                            chatRoomsRef.child(singleChatRoomNode + '/_unreadByUser_2').set(count);
                        }
                        console.log('#ChatScreen: count updated.............');
                        _this._onMessageDeliverSuccess(singleChatRoomNode);
                    });
                });
            }

        });
    }

    _showMessagesInListView() {
        let currUserKey = '';
        let chatNode = this.state.currChatRoomKey;
        let userArr = chatNode.split('_');
        let userName1 = userArr[0];
        let userName2 = userArr[1];
        console.log('#ChatScreen: showMessageInListView userNames', userName1, userName2);
        let finalUserName = HomeTab._getCurrentUserNodeKey();
        if (finalUserName === userName1) {
            currUserKey = userName1;
        } else {
            currUserKey = userName2;
        }
        return (
            this.state.messagesArr.map((item, index) => {
                if (item) {
                    if (item._msgSender === currUserKey) {
                        console.log('## _showMessagesInListView if = ', item._msgSender, currUserKey, item._msgType);
                        if (item._msgType === 'text') {
                            return (
                                this._renderMsgViewSentByYou(item, index)
                            )
                        } else {
                            return (
                                this._renderMsgImageViewSentByYou(item, index)
                            )
                        }
                    } else {
                        console.log('## _showMessagesInListView else = ', item._msgSender, currUserKey, item._msgType);
                        if (item._msgType === 'text') {
                            return (
                                this._renderMsgViewSentByOther(item, index)
                            )
                        } else {
                            return (
                                this._renderMsgImageViewSentByOther(item, index)
                            )
                        }
                    }
                }
            })
        )
    }

    _renderMsgViewSentByYou(item, index) {
        let msgTxt = item._msgTxt;
        if (msgTxt)
            return (
                <View key={index} style={[styles.sentMessageView]}>
                    <Text style={{fontWeight: 'bold'}}>{msgTxt}</Text>
                </View>
            )
    }

    _renderMsgViewSentByOther(item, index) {
        let msgTxt = item._msgTxt;
        if (msgTxt)
            return (
                <View key={index} style={styles.receivedMessageView}>
                    <Text style={{fontWeight: 'bold'}}>{msgTxt}</Text>
                </View>
            )
    }

    _renderMsgImageViewSentByYou(item, index) {
        let msgPic = item._msgImageData;
        if (msgPic)
            return (
                <View key={index}
                      style={{marginRight: 16, marginBottom: 16, justifyContent: 'flex-end', alignItems: 'flex-end'}}>
                    <Image
                        style={styles.styleSentMsgImageView}
                        source={(msgPic && msgPic !== null) ? msgPic : require('../assets/bg/bg.png')}/>
                </View>
            )
    }

    _renderMsgImageViewSentByOther(item, index) {
        let msgPic = item._msgImageData;
        if (msgPic)
            return (
                <View key={index} style={{marginLeft: 16, marginBottom: 16, justifyContent: 'flex-start'}}>
                    <Image
                        style={styles.styleReceivedMsgImageView}
                        source={(msgPic && msgPic !== null) ? msgPic : require('../assets/bg/bg.png')}/>
                </View>
            )
    }

    selectImage() {
        var storageRef = firebase.storage().ref();
        var imageRef = storageRef.child('images/image1.jpg');
        console.log('## Image ref with full path = ' + imageRef);
        var metadata = {
            contentType: 'image/jpeg',
        };
        console.log('##selectImage function ............................');
        ImagePicker.showImagePicker(options, (response) => {
            console.log('##Response = ', response);
            if (response.didCancel) {
                console.log('##User cancelled image picker');
            }
            else if (response.error) {
                console.log('##ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('##User tapped custom button: ', response.customButton);
            }
            else {

                //let source = response.uri;
                //let sourceBase64 = {uri: 'data:image/jpeg;base64,' + response.data};
                let uriSource = {uri: response.uri};
                this.sendMessage('', uriSource);
            }
        });
    }

    render() {
        const isEnable = this.state.message.length > 0;
        return (
            <View style={{flex: 1, backgroundColor: '#F6F5F7'}}>
                <ScrollView style={{marginTop: 8}}
                            ref="scroller"
                            scrollEventThrottle={16}
                            onScroll={this.handleScroll}
                            onLayout={this.handleLayout}
                            keyboardShouldPersistTaps={'always'}
                            onContentSizeChange={this.handleContentChange}>
                    {this._showMessagesInListView()}
                </ScrollView>

                <View style={{flexDirection: 'row', backgroundColor: '#FFFFFF'}}>
                    <TouchableOpacity onPress={this.selectImage.bind(this)}>
                        <View style={{height: 52, width: width * 0.2, alignItems: 'center', justifyContent: 'center'}}>
                            <Image source={require('../assets/camera/icon-camera-message.png')}
                                   style={{height: 30, width: 40}}/>
                        </View>
                    </TouchableOpacity>
                    <TextInput
                        style={{height: 50, width: width * 0.6}}
                        ref={'chatBox'}
                        placeholder={"Write a message..."}
                        placeholderTextColor={'#C6C6C6'}
                        onChangeText={(message) => this.setState({message: message})}
                        underlineColorAndroid={'transparent'}
                        value={this.state.message}
                    />
                    <TouchableOpacity onPress={() => this.sendMessage(this.state.message, null)} disabled={!isEnable}>
                        <View style={{
                            height: height * 0.085,
                            width: width * 0.2,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <Image source={require('../assets/send/send.png')}
                                   style={{height: 30, width: 30, opacity: isEnable ? 1 : 0.5}}/>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    _updateUserStatusToOffline(singleChatRoomNode) {
        let chatRoomsRef = firebase.database().ref(keyNodeChatRooms);
        chatRoomsRef.child(singleChatRoomNode).once('value', function (snapshot) {
            if (snapshot.exists()) {
                printLog('ChatScreen: node ' + singleChatRoomNode + ' already exists.');
                let singleChatRoomData = snapshot.val();
                let roomStatus = singleChatRoomData._room_status;
                let user1Key = roomStatus._user_1_id;
                let currUserKey = HomeTab._getCurrentUserNodeKey();
                if (user1Key === currUserKey) {
                    firebase.database().ref(keyNodeChatRooms + singleChatRoomNode + keySingleChatRoomStatus + '/_isUser_1_online')
                        .set(false).then(() => {
                        printLog('ChatScreen: onBackPress user 1 status set to offline.');
                    });
                } else {
                    firebase.database().ref(keyNodeChatRooms + singleChatRoomNode + keySingleChatRoomStatus + '/_isUser_2_online')
                        .set(false).then(() => {
                        printLog('ChatScreen: onBackPress user 2 status set to offline.');
                    });
                }
            } else {
                printLog('ChatScreen: node ' + singleChatRoomNode + ' does not exists.');
            }
        });
    }

    _fetchCurrChatRoomStatus(singleChatRoomNode) {
        let _this = this;
        let chatRoomsRef = firebase.database().ref(keyNodeChatRooms);
        chatRoomsRef.child(singleChatRoomNode).once('value', function (snapshot) {
            if (snapshot.exists()) {
                printLog('ChatScreen: node ' + singleChatRoomNode + ' already exists.');
                let singleChatRoomData = snapshot.val();
                let user1Key = singleChatRoomData._user_1_id;
                let user2Key = singleChatRoomData._user_2_id;
                let user1Status = singleChatRoomData._isUser_1_online;
                let user2Status = singleChatRoomData._isUser_2_online;
                let currUserKey = HomeTab._getCurrentUserNodeKey();
                if (user1Key === currUserKey) {
                    firebase.database().ref(keyNodeChatRooms + singleChatRoomNode + '/_isUser_1_online')
                        .set(false).then(() => {
                        printLog('ChatScreen: onBackPress user 1 status set to offline.');
                    });
                } else {
                    firebase.database().ref(keyNodeChatRooms + singleChatRoomNode + '/_isUser_2_online')
                        .set(false).then(() => {
                        printLog('ChatScreen: onBackPress user 2 status set to offline.');
                    });
                }
            } else {
                printLog('ChatScreen: node ' + singleChatRoomNode + ' does not exists.');
            }
        });
    }
}

const styles = StyleSheet.create({
    receivedMessageView: {
        maxWidth: width * 0.6,
        backgroundColor: '#E6E5E4',
        alignSelf: 'flex-start',
        padding: 8,
        marginLeft: 16,
        marginBottom: 16,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        borderBottomRightRadius: 8,
    },
    sentMessageView: {
        maxWidth: width * 0.6,
        backgroundColor: '#D9D3EF',
        padding: 8,
        alignSelf: 'flex-end',
        marginRight: 16,
        marginBottom: 16,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        borderBottomLeftRadius: 8,
    },
    styleSentMsgImageView: {
        borderColor: '#D9D3EF',
        borderWidth: 2,
        width: 120,
        height: 120,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        borderBottomLeftRadius: 8,
    },
    styleReceivedMsgImageView: {
        borderColor: '#E6E5E4',
        borderWidth: 2,
        width: 120,
        height: 120,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        borderBottomRightRadius: 8,
    },

});

