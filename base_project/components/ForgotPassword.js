import React, {Component} from 'react';
import {
    Image,
    ImageBackground,
    Keyboard,
    KeyboardAvoidingView,
    StatusBar,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View
} from 'react-native';

import * as firebase from "firebase";
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from "react-native-linear-gradient";
import appStyles from "../app_delegates/AppStyles";
import {showToastGlobal} from "./BaseClass";

class ForgotPassword extends Component {

    static navigatorStyle = {
        drawUnderNavBar: true,
        topBarElevationShadowEnabled: false,
        statusBarHidden: true,
        navBarTransparent: true,
        disabledBackGesture: false,
        navBarHidden: false,
        navBarTranslucent: true
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
        this.state = {
            user_email: '',
            spinner: false,
        }
    }

    onNavigatorEvent(event) {
        if (event.type === 'NavBarButtonPress') {
            if (event.id === 'back') {
                this._popScreenFromStack('ForgotPassword');
            }
        }
    }

    _popScreenFromStack(screenName) {
        this.props.navigator.pop({
            screen: screenName,
        });
    }

    _onResetPassword(email) {
        if (email.length > 0) {
            let _this = this;
            _this._showSpinner();
            firebase.auth().sendPasswordResetEmail(
                email, null)
                .then(function () {
                    // Password reset email sent.
                    showToastGlobal('Password reset link sent to your email.');
                    _this._dismissSpinner();
                })
                .catch(function (error) {
                    showToastGlobal('Error: ' + error);
                    _this._dismissSpinner();
                });
        } else {
            showToastGlobal('Provide an email first.');
        }
    }

    _showSpinner() {
        this.setState({spinner: true});
    }

    _dismissSpinner() {
        this.setState({spinner: false});
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={{flex: 1}}>
                    <ImageBackground source={require('../assets/bg/bg.png')} style={{flex: 1}}>
                        <View style={appStyles.blackOverlayView}>
                            <KeyboardAvoidingView
                                style={appStyles.styleAvoidKeyboard}>
                                <StatusBar hidden={true}/>
                                <View style={{height: 10}}/>
                                <View style={[appStyles.styleAlignItemsCenter]}>
                                    <Image source={require('../assets/logo/logo.png')}/>
                                </View>
                                <View style={{height: 50}}/>
                                <View style={{padding: 10}}>
                                    <View style={appStyles.styleViewTxtInputBox}>
                                        <TextInput style={appStyles.styleTxtInputTxt}
                                                   maxLength={40}
                                                   returnKeyType="done"
                                                   keyboardType={'email-address'}
                                                   placeholder="Enter your email"
                                                   placeholderTextColor={'#D4D4D4'}
                                                   underlineColorAndroid="transparent"
                                                   value={this.state.user_email}
                                                   onChangeText={(email) => this.setState({user_email: email})}
                                        />
                                    </View>
                                    <View style={appStyles.styleViewGradientBtn}>
                                        <TouchableWithoutFeedback
                                            onPress={() => this._onResetPassword(this.state.user_email)}>
                                            <LinearGradient
                                                start={{x: 0.0, y: 0.0}} end={{x: 1.0, y: 1.0}}
                                                locations={[0, 0.3, 1.0]}
                                                colors={['#8B5BE6', '#8B5BE6', '#54D2E1']}
                                                style={{flex: 1, padding: 12, borderRadius: 4,}}>
                                                <View>
                                                    <Text style={appStyles.styleGradientBtnTxt}>
                                                        Reset Password
                                                    </Text>
                                                </View>
                                            </LinearGradient>
                                        </TouchableWithoutFeedback>
                                    </View>
                                </View>
                                <Spinner
                                    color={'#54D2E1'}
                                    visible={this.state.spinner}
                                    cancelable={false}/>
                            </KeyboardAvoidingView>
                        </View>
                    </ImageBackground>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

export default ForgotPassword;