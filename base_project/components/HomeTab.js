import React, {Component} from 'react';
import {AsyncStorage, Image, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View,} from 'react-native';
import * as firebase from 'firebase';
import Spinner from 'react-native-loading-spinner-overlay';
import {
    keyIsUserLoggedIn,
    keyNodeAllUsers,
    keyNodeChatRooms,
    keySingleChatRoomStatus,
    printLog,
    showToastGlobal
} from "./BaseClass";
import {Navigation} from "react-native-navigation";
import appStyles from "../app_delegates/AppStyles";

class HomeTab extends Component {

    static navigatorButtons = {
        rightButtons: [
            {
                id: 'logout',
                icon: require('../assets/gender/gender-icon.png'),
            },
        ],
    };

    static navigatorStyle = {
        drawUnderNavBar: false,
        navBarBackgroundColor: '#FFF',
        topBarElevationShadowEnabled: true,
        statusBarHidden: false,
        navBarComponentAlignment: 'center',
        statusBarColor: '#000000',
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
        this.state = {
            spinner: false,
            currentUserName: '',
            usersArr: [],
            currUserUid: '',
        };
    }

    _showSpinner() {
        this.setState({spinner: true});
    }

    _dismissSpinner() {
        this.setState({spinner: false});
    }

    _popScreenFromStack(screenName) {
        this.props.navigator.pop({
            screen: screenName,
        });
    }

    _pushScreenIntoStack(screenName, userItem) {
        this.props.navigator.push({
            screen: screenName,
            passProps: {selectedUserItem: userItem},
        });
    }

    static _getCurrentUserNodeKey() {
        let currentUserFromAuth = firebase.auth().currentUser;
        return currentUserFromAuth.uid;
    }

    static _getUsersChatNodeKey(currUserKey, selectedUserKey) {
        let arr = [];
        arr.push(currUserKey);
        arr.push(selectedUserKey);
        arr.sort((currUserKey, selectedUserKey) => currUserKey.localeCompare(selectedUserKey));
        return arr[0] + '_' + arr[1];
    }

    logoutFromFireBase() {
        try {
            firebase.auth().signOut()
                .then(() => {
                    console.log('Logout success.');
                    try {
                        AsyncStorage.setItem(keyIsUserLoggedIn, 'No').then(() => {
                            console.log('logout success at dashboard.');
                            Navigation.startSingleScreenApp({
                                screen: {
                                    screen: 'LoginScreen',
                                    navigatorStyle: {},
                                    navigatorButtons: {}
                                },
                            });
                        });
                    } catch (error) {
                        console.log('Error while saving isUserLoggedIn at dashboard.');
                    }
                });
        } catch (error) {
            showToastGlobal('Logout ' + error.toString());
        }
    }

    componentDidMount() {
        this.props.navigator.setTitle({
            title: "Dashboard"
        });
        this.props.navigator.setStyle({
            navBarBackgroundColor: '#FFF',
            navBarComponentAlignment: 'center',
        });

        let currentUserFromAuth = firebase.auth().currentUser;
        this._fetchAllUsersFromFireBase(currentUserFromAuth.uid);
    }

    onNavigatorEvent(event) {
        if (event.type === 'NavBarButtonPress') {
            if (event.id === 'logout') {
                this.logoutFromFireBase();
                //showToastGlobal('Logout feature to be implemented.');
            }
        }
    }

    _fetchAllUsersFromFireBase(uid) {
        let _this = this;
        let usersRef = firebase.database().ref(keyNodeAllUsers);
        _this.setState({currUserUid: uid});
        usersRef.child(uid).once('value', function (snapshot) {

            let currUserProfile = snapshot.val().profile;
            let currUserGender = currUserProfile._userSex;

            usersRef.on('value', function (snapshot) {
                let data = snapshot.val();
                if (data != null) {
                    let dataArr = [];
                    snapshot.forEach(function (childSnapshot) {
                        if (childSnapshot.key !== uid) {
                            console.log('#HomeTab: not currUser', childSnapshot.key);
                            let childData = childSnapshot.val();
                            let _profile = childData.profile;
                            if (_profile) {
                                let childUserGender = _profile._userSex;
                                if (childUserGender !== currUserGender) {
                                    console.log('#HomeTab: user of other gender found', childUserGender);
                                    let name = childData.profile._userName;
                                    let location = childData.profile._userLocation;
                                    let gender = childData.profile._userSex;
                                    let age = childData.profile._userAge;
                                    let about = childData.profile._userAbout;
                                    let interestedIn = childData.profile._userInterestInMen;
                                    let _profilePic = childData.profile._userProfilePic;
                                    dataArr.push({
                                        _name: name,
                                        _age: age,
                                        _gender: gender,
                                        _location: location,
                                        _about: about,
                                        _interestedIn: interestedIn ? 'Male' : 'Female',
                                        _key: childSnapshot.key,
                                        _userProfilePic: _profilePic
                                    });
                                } else {
                                    console.log('#HomeTab: user of other gender not found',);
                                }
                            }
                        }
                        _this.setState({usersArr: dataArr});
                    });
                } else {
                    console.log('No user node found at Dashboard.');
                }
            });
        }).then(() => {
            console.log('#HomeTab: currUserProfile: then()');
        });

    }

    _pushNewChatNodeInChatRooms(chatNodeToBeCreated, currUserKey, selectedUserKey, item) {
        let chatRoomsRef = firebase.database().ref(keyNodeChatRooms);
        chatRoomsRef.child(chatNodeToBeCreated).set({
            _unreadByUser_1: 0,
            _unreadByUser_2: 0,
        }).then(() => {
            chatRoomsRef.child(chatNodeToBeCreated + keySingleChatRoomStatus)
                .set({
                    _user_1_id: currUserKey,
                    _user_2_id: selectedUserKey,
                    _isUser_1_online: false,
                    _isUser_2_online: false,
                    _lastMsg: {},
                }).then(() => {
                this._pushScreenIntoStack('MatchScreen', item);
            });
        });
    }

    _checkIfNodeExists(nodeToBeChecked) {
        let chatRoomsRef = firebase.database().ref(keyNodeChatRooms);
        chatRoomsRef.child(nodeToBeChecked).once('value', function (snapshot) {
            if (snapshot.exists()) {
                printLog('HomeTab: node ' + nodeToBeChecked + ' already exists.');
            } else {
                printLog('HomeTab: node ' + nodeToBeChecked + ' does not exists.');
            }
        });
    }

    _onItemClick(item) {
        this._showSpinner();
        this._checkUserMatchState(this.state.currUserUid, item._key, item, true);
    }

    async _checkUserMatchState(currUserKey, selectedUserKey, item, isClicked) {
        let _this = this;
        let chatNodeToBeCreated = HomeTab._getUsersChatNodeKey(currUserKey, selectedUserKey);
        await firebase.database().ref(keyNodeAllUsers + currUserKey + '/' + chatNodeToBeCreated).on('value', function (snapshot) {
            let data = snapshot.val();
            _this._checkIfNodeExists(chatNodeToBeCreated);
            if (data !== null) {
                let a = data._me;
                let b = data._other;
                _this.setState({likedByMe: a, likedByOther: b});
                if (a && b) {
                    _this._dismissSpinner();
                    let chatRoomsRef = firebase.database().ref(keyNodeChatRooms);
                    chatRoomsRef.child(chatNodeToBeCreated).once('value', function (snapshot) {
                        if (snapshot.exists()) {
                            _this._pushScreenIntoStack('MatchScreen', item);
                        } else {
                            _this._pushNewChatNodeInChatRooms(chatNodeToBeCreated, currUserKey, selectedUserKey, item);
                        }
                    });
                } else {
                    _this._dismissSpinner();
                    if (isClicked) _this._pushScreenIntoStack('AboutUserScreen', item);
                }
            } else {
                _this._dismissSpinner();
                if (isClicked) _this._pushScreenIntoStack('AboutUserScreen', item);
            }
            isClicked = !isClicked;
        });
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#f2f2f2',}}>
                <ScrollView
                    keyboardShouldPersistTaps={'handled'}
                    style={styles.scrollViewContainer}
                    showsVerticalScrollIndicator={false}>
                    {this._fetchUsers()}
                    <View style={{height: 12}}/>
                    <Spinner
                        color={'#54D2E1'}
                        visible={this.state.spinner}
                        cancelable={false}/>
                </ScrollView>
            </View>
        );
    }

    _fetchUsers() {
        return (
            this.state.usersArr.map((item, index) => {
                return (
                    this._renderBaseCardViewForUser(item, index)
                );
            })
        )
    }

    _renderBaseCardViewForUser(item, index) {
        return (
            <View key={index} style={appStyles.styleAlignItemsCenter}>
                <TouchableWithoutFeedback
                    onPress={() => this._onItemClick(item)}>
                    <View style={styles.styleHomeTabCardBaseView}>
                        <View style={{flex: 1,}}>
                            <View>
                                <Text style={styles.styleTxtAboutMe}>
                                    About me
                                </Text>
                                <Text numberOfLines={5}>
                                    {item._about}
                                </Text>
                            </View>
                        </View>

                        <View style={styles.styleVerticalLine}/>

                        <View style={{flex: 1}}>

                            <View style={styles.styleViewTxtUserName}>
                                <Text
                                    ellipsizeMode={'tail'}
                                    numberOfLines={1}
                                    style={styles.styleTxtUserName}>
                                    {item._name}
                                </Text>
                            </View>
                            <View style={styles.styleViewTxtOtherInfo}>
                                <Text style={styles.styleTxtOtherInfo}>
                                    {item._age} years
                                </Text>
                            </View>


                            <View style={{flexDirection: 'row', marginTop: 6}}>
                                <View style={{marginRight: 6}}>
                                    <Image
                                        source={require('../assets/gender/gender-icon.png')}
                                    />
                                </View>
                                <View>
                                    <Text style={styles.styleTxtOtherInfo}>
                                        {item._gender}
                                    </Text>
                                </View>
                            </View>
                            <View style={{flexDirection: 'row', marginTop: 4}}>
                                <View style={{marginRight: 6, alignItems: 'center', justifyContent: 'center'}}>
                                    <Image
                                        source={require('../assets/location/location-icon.png')}
                                    />
                                </View>
                                <View>
                                    <Text style={styles.styleTxtOtherInfo}>
                                        {item._location}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scrollViewContainer: {
        marginHorizontal: 10,
        flex: 1,
    },
    styleHomeTabCardBaseView: {
        flex: 1,
        minHeight: 115,
        maxHeight: 125,
        backgroundColor: 'white',
        borderRadius: 4,
        borderColor: '#FFF',
        marginTop: 12,
        marginLeft: 2,
        marginRight: 2,
        padding: 8,
        flexDirection: 'row',
    },
    styleVerticalLine: {
        borderLeftColor: '#f0f0f0',
        borderLeftWidth: 1,
        marginLeft: 8,
        marginRight: 8,
    },
    styleViewTxtUserName: {
        flex: 1,
        alignItems: 'flex-start',
    },
    styleTxtUserName: {
        fontSize: 24,
        color: '#4A4848',
        fontWeight: 'bold',
    },
    styleTxtAboutMe: {
        fontSize: 18,
        color: '#7A7A7A',
        fontWeight: 'bold',
    },
    styleViewTxtOtherInfo: {
        flex: 1,
        alignItems: 'flex-start',
    },
    styleTxtOtherInfo: {
        fontSize: 14,
        color: '#A3A3A3',
    },
});

export default HomeTab;