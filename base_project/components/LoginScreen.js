import React, {Component} from 'react';
import {
    AsyncStorage,
    Image,
    ImageBackground,
    KeyboardAvoidingView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View,
} from 'react-native';

import * as firebase from "firebase";
import Spinner from 'react-native-loading-spinner-overlay';
import {Navigation} from "react-native-navigation";
import {keyIsUserLoggedIn, keyNodeAllUsers, keyUserUID, showToastGlobal} from "./BaseClass";
import LinearGradient from "react-native-linear-gradient";
import appStyles from "../app_delegates/AppStyles";

class LoginScreen extends Component {

    static navigatorStyle = {
        drawUnderNavBar: true,
        navBarBackgroundColor: 'transparent',
        topBarElevationShadowEnabled: false,
        statusBarHidden: true,
        navBarHidden: true,
    };

    handlePress = (call_type) => {
        this._pushScreenIntoStack(call_type === 'SignUp' ? 'SignUpScreen' : 'ForgotPassword', 'Push');
    };

    _pushScreenIntoStack(screenName, type) {
        if (type === 'Push') {
            this.props.navigator.push({
                screen: screenName,
            });
        } else {
            this.props.navigator.resetTo({
                screen: screenName,
            });
        }
    }

    constructor() {
        super();
        this.state = {
            user_email: '',
            user_password: '',
            spinner: false,
            loading: true,
            current_user: '',
        };
    }

    _funcFireBaseLogin(email, pass) {
        let _this = this;
        if (_this.state.user_email.length > 0) {
            if (_this.state.user_password.length > 5) {
                _this._showSpinner();
                firebase.auth().signInWithEmailAndPassword(email, pass)
                    .then(function (user) {
                        console.log('LoginScreen: signIn success saving isUserLoggedIn... ',);
                        _this._saveIsUserLoggedIn(user.uid);
                        //_this._refreshAllValuesToBlank();
                    }).catch(function (error) {
                    _this._dismissSpinner();
                    showToastGlobal('Error:\n' + error.toString());
                });
            } else {
                _this._dismissSpinner();
                showToastGlobal('Password must be greater than or equal to 6 characters')
            }
        } else {
            _this._dismissSpinner();
            showToastGlobal('Email and password fields cannot be blank.')
        }
    }

    _showSpinner() {
        this.setState({spinner: true});
    }

    async _goToQuestionnaireScreen(uid) {
        let _this = this;
        await firebase.database().ref(keyNodeAllUsers + uid).on('value', function (snapshot) {
            let data = snapshot.val();
            if (data != null) {
                console.log('LoginScreen: UserData at login node:', data);
                let isAnswered = data.isAnswered;
                let isProfileComplete = data.isProfileComplete;
                console.log('LoginScreen:', isAnswered, isProfileComplete);
                if (isProfileComplete) {
                    _this._startTabBasedDashboard();
                } else {
                    _this._dismissSpinner();
                    _this._pushScreenIntoStack(isAnswered ? 'ProfileScreen' : 'QuestionnaireScreen', 'ResetTo');
                }
            } else {
                _this._dismissSpinner();
                console.log('LoginScreen: No user node found.');
            }
        });
    }

    _startTabBasedDashboard() {
        this._dismissSpinner();
        Navigation.startTabBasedApp({
            tabs: [
                {
                    tabIndex: 0,
                    label: 'Home',
                    screen: 'HomeTab',
                    icon: require('../assets/home-tab-normal/home-tab.png'),
                    selectedIcon: require('../assets/home-tab-active/home-tab-active.png'),
                    title: 'Home'
                },
                {
                    tabIndex: 1,
                    label: 'Messages',
                    screen: 'MessagesTab',
                    icon: require('../assets/message-tab-normal/message-tab.png'),
                    selectedIcon: require('../assets/message-tab-active/message-tab-active.png'),
                    title: 'Messages'
                },
                {
                    tabIndex: 2,
                    label: 'Profile',
                    screen: 'ProfileTab',
                    icon: require('../assets/profile-tab-normal/profile-tab.png'),
                    selectedIcon: require('../assets/profile-tab-active/profile-tab-active.png'),
                    title: 'Profile'
                }
            ],
            tabsStyle: {
                tabBarButtonColor: '#b1b1b1',
                tabBarSelectedButtonColor: '#7a54d3',
                tabBarBackgroundColor: '#FFF',
                initialTabIndex: 0,
            },
            appStyle: {
                tabBarBackgroundColor: 'white',
                tabBarButtonColor: '#b1b1b1',
                tabBarSelectedButtonColor: '#7a54d3',
                tabBarTranslucent: false,
                tabFontFamily: 'Cabin-SemiBold',
                tabFontSize: 16,
                selectedTabFontSize: 16,
                hideBackButtonTitle: true,
            },
            // drawer: {
            //     left: {
            //         screen: 'ProfileTab',
            //         fixedWidth: 700,
            //     },
            // },
        });
    }


    _saveIsUserLoggedIn(uid) {
        let _this = this;
        try {
            AsyncStorage.setItem(keyIsUserLoggedIn, 'Yes').then(() => {
                AsyncStorage.setItem(keyUserUID, uid).then(() => {
                    console.log('LoginScreen: isUserLoggedIn saved. Now go to next screen.');
                    _this._goToQuestionnaireScreen(uid);
                    //_this._dismissSpinner();
                    //showToastGlobal('You are now logged in.');
                });
            });
        } catch (error) {
            _this._dismissSpinner();
            console.log('Error while saving isUserLoggedIn.');
        }
    }

    _dismissSpinner() {
        this.setState({spinner: false});
    }

    _refreshAllValuesToBlank() {
        this.setState({user_email: '', user_password: ''});
    }

    render() {
        return (
            <ImageBackground source={require('../assets/bg/bg.png')} style={{flex: 1}}>
                <View style={appStyles.blackOverlayView}>
                    <ScrollView
                        contentContainerStyle={styles.scrollViewContainer}
                        showsVerticalScrollIndicator={false}
                        keyboardShouldPersistTaps={'handled'}>
                        <KeyboardAvoidingView style={appStyles.styleAvoidKeyboard}>
                            <StatusBar hidden={true}/>
                            <View style={{flex: 2, justifyContent: 'flex-end'}}>
                                <View style={[appStyles.styleAlignItemsCenter]}>
                                    <Image source={require('../assets/logo/logo.png')}/>
                                </View>

                            </View>

                            <View style={{flex: 4, justifyContent: 'center'}}>
                                <View style={appStyles.styleViewTxtInputBox}>
                                    <TextInput style={appStyles.styleTxtInputTxt}
                                               placeholder="Enter your email"
                                               maxLength={40}
                                               returnKeyType="done"
                                               keyboardType={'email-address'}
                                               placeholderTextColor={'#D4D4D4'}
                                               underlineColorAndroid="transparent"
                                               value={this.state.user_email}
                                               onChangeText={(email) => this.setState({user_email: email})}
                                    />
                                </View>
                                <View style={appStyles.styleViewTxtInputBox}>
                                    <TextInput style={appStyles.styleTxtInputTxt}
                                               placeholder="Enter your password"
                                               placeholderTextColor={'#D4D4D4'}
                                               maxLength={16}
                                               returnKeyType="done"
                                               keyboardType={'default'}
                                               underlineColorAndroid="transparent"
                                               secureTextEntry={true}
                                               value={this.state.user_password}
                                               onChangeText={(password) => this.setState({user_password: password})}
                                    />
                                </View>
                                <View style={appStyles.styleViewGradientBtn}>
                                    <TouchableWithoutFeedback
                                        onPress={() => this._funcFireBaseLogin(this.state.user_email, this.state.user_password)}>
                                        <LinearGradient
                                            start={{x: 0.0, y: 0.0}} end={{x: 1.0, y: 1.0}}
                                            locations={[0, 0.3, 1.0]}
                                            colors={['#8B5BE6', '#8B5BE6', '#54D2E1']}
                                            style={{flex: 1, padding: 12, borderRadius: 4,}}
                                        >
                                            <View>
                                                <Text style={appStyles.styleGradientBtnTxt}>
                                                    SIGN IN
                                                </Text>
                                            </View>
                                        </LinearGradient>
                                    </TouchableWithoutFeedback>
                                </View>
                                <View style={[{alignItems: 'flex-end'}]}>
                                    <TouchableWithoutFeedback onPress={() => this.handlePress('ForgotPassword')}>
                                        <View>
                                            <Text style={styles.forgotPassword}>
                                                Forgot Password?
                                            </Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>

                            <View style={{flex: 2, justifyContent: 'center'}}>
                                <View style={appStyles.styleAlignItemsCenter}>
                                    <Text style={{
                                        textAlign: 'center',
                                        color: '#E4E4E4', fontSize: 22, marginTop: 8
                                    }}>
                                        Don't have an account yet?
                                    </Text>
                                </View>
                                <View style={appStyles.styleAlignItemsCenter}>
                                    <TouchableWithoutFeedback
                                        onPress={() => this.handlePress('SignUp')}>
                                        <View>
                                            <Text style={styles.createAccount}>
                                                Create an account
                                            </Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                            <Spinner
                                color={'#54D2E1'}
                                visible={this.state.spinner}
                                cancelable={false}/>
                        </KeyboardAvoidingView>
                    </ScrollView>
                </View>
            </ImageBackground>
        );
    }
}

firebase.initializeApp({
    apiKey: "AIzaSyC7QStYUWXe2vonX5zCN6bU_l5NTwi--qg",
    authDomain: "youforyoudev.firebaseapp.com",
    databaseURL: "https://youforyoudev.firebaseio.com",
    storageBucket: "youforyoudev.appspot.com",
    projectId: "youforyoudev",
    messagingSenderId: "592755210865"
});

const styles = StyleSheet.create({
    scrollViewContainer: {
        marginHorizontal: 10, marginTop: 10, marginBottom: 2, flex: 1
    },
    forgotPassword: {
        height: 24, textAlign: 'right', color: '#E4E4E4',
        fontSize: 20, marginTop: 12, marginRight: 20,
    },
    createAccount: {
        height: 24, textAlign: 'center', color: '#0D92CA',
        fontSize: 22, marginTop: 4, marginBottom: 8,
    },
});

export default LoginScreen;
