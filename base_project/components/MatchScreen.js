import React, {Component} from 'react';
import {Image, Text, TouchableWithoutFeedback, View} from 'react-native';
import appStyles from "../app_delegates/AppStyles";
import LinearGradient from "react-native-linear-gradient";

class MatchScreen extends Component {

    static navigatorStyle = {
        drawUnderNavBar: false,
        navBarBackgroundColor: '#FFF',
        topBarElevationShadowEnabled: true,
        statusBarHidden: false,
        navBarComponentAlignment: 'center',
        statusBarColor: '#000000',
    };

    constructor() {
        super();
        this.state = {
            spinner: false,
            selectedUserItem: '',
        }
    }

    componentDidMount() {
        this.props.navigator.setTitle({
            title: "Match"
        });
        let userItem = this.props.selectedUserItem;
        console.log('SelectedUser at MatchScreen', userItem);
        this.setState({selectedUserItem: userItem});
    }

    _showSpinner() {
        this.setState({spinner: true});
    }

    _dismissSpinner() {
        this.setState({spinner: false});
    }

    render() {
        return (

            <View style={[appStyles.styleBaseBackgroundView, {flex: 1}]}>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-end', flexDirection: 'column'}}>
                    <Text style={{fontSize: 36}}>
                        It's a Match!
                    </Text>
                </View>
                <View style={{flex: 3, justifyContent: 'center', alignItems: 'center'}}>
                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: 100,
                        height: 100,
                        borderRadius: 50,
                        backgroundColor: 'white',
                    }}>
                        <View style={{
                            width: 95,
                            height: 95,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 50,
                            backgroundColor: '#f6f6f6'
                        }}>
                            <Image
                                style={{width: 40, height: 40}}
                                source={require('../assets/thumb-like/thumb.png')}/>
                        </View>
                    </View>
                </View>
                <View style={{flex: 1, alignItems: 'center'}}>
                    <View style={appStyles.styleViewGradientBtn}>
                        <TouchableWithoutFeedback
                            onPress={() => this._onViewProfileBtnClick()}>
                            <LinearGradient
                                start={{x: 0.0, y: 0.0}} end={{x: 1.0, y: 1.0}}
                                locations={[0, 0.3, 1.0]}
                                colors={['#8B5BE6', '#8B5BE6', '#54D2E1']}
                                style={{flex: 1, padding: 12, borderRadius: 4,}}>
                                <View>
                                    <Text style={appStyles.styleGradientBtnTxt}>
                                        View Profile
                                    </Text>
                                </View>
                            </LinearGradient>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </View>
        );
    }

    _onViewProfileBtnClick() {
        let userItem = this.state.selectedUserItem;
        console.log('OnViewProfileClick:', userItem);
        this.props.navigator.push({
            screen: 'ViewUserProfileScreen',
            passProps: {selectedUserItem: userItem},
        });
    }
}

export default MatchScreen;
