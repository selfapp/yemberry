import React, {Component} from 'react';
import {Dimensions, Image, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View} from 'react-native';
import * as firebase from "firebase";
import {keyNodeAllUsers, keyNodeChatRooms, keySingleChatRoomStatus, printLog} from "./BaseClass";
import HomeTab from "./HomeTab";

var {height, width} = Dimensions.get('window');
var moment = require('moment');

class MessagesTab extends Component {

    constructor() {
        super();
        this.state = {
            condition: false,
            chatUsersArr: [],
            chatNodeKey: '',
        }
    }

    static navigatorStyle = {
        drawUnderNavBar: false,
        navBarBackgroundColor: '#FFF',
        topBarElevationShadowEnabled: true,
        statusBarHidden: false,
        navBarComponentAlignment: 'center',
        statusBarColor: '#000000',
    };

    componentWillMount() {
        this.props.navigator.setTitle({
            title: "Messages",
        });
        this._fetchAllChatRooms();
    }

    _fetchAllChatRooms() {
        let _this = this;
        console.log('#MessagesTab: _fetchAllChatRooms called.',);
        let chatRoomsRef = firebase.database().ref(keyNodeChatRooms);
        chatRoomsRef.on('value', function (snapshot) {
            let chatRoomsArr = [];
            snapshot.forEach(function (childSnapshot) {
                let currChatNode = childSnapshot.key;
                let singleChatRoomData = childSnapshot.val();
                let roomStatus = singleChatRoomData._room_status;
                console.log('#MessagesTab: _fetchAllChatRooms roomStatus:', roomStatus, singleChatRoomData);
                if (roomStatus) {
                    let user1_id = roomStatus._user_1_id;
                    let user2_id = roomStatus._user_2_id;
                    let currUserId = HomeTab._getCurrentUserNodeKey();
                    let usersNodeRef = firebase.database().ref(keyNodeAllUsers);

                    let unreadMsgCountUser_1 = singleChatRoomData._unreadByUser_1;
                    let unreadMsgCountUser_2 = singleChatRoomData._unreadByUser_2;

                    let _msgTxt = '';
                    let _lastMsgTime = '';
                    let _msgReadByUser_1 = '';
                    let _msgReadByUser_2 = '';
                    let lastMsgData = roomStatus._lastMsg;
                    if (lastMsgData) {
                        _msgTxt = lastMsgData._msgTxt;
                        _lastMsgTime = lastMsgData._msgSendTime;
                        _msgReadByUser_1 = lastMsgData._readByUser_1;
                        _msgReadByUser_2 = lastMsgData._readByUser_2;
                    }
                    console.log('#MessagesTab: at _fetchAllChatRooms: unread counts', unreadMsgCountUser_1, unreadMsgCountUser_2);
                    if (currUserId === user1_id) {
                        try {
                            usersNodeRef.child(user2_id + '/profile').on('value', function (snapshot) {
                                let profileData = snapshot.val();
                                let name = profileData._userName;
                                let location = profileData._userLocation;
                                let gender = profileData._userSex;
                                let age = profileData._userAge;
                                let about = profileData._userAbout;
                                let _profilePic = profileData._userProfilePic;
                                console.log('#MessagesTab: Data found at _fetchAllChatRooms:', name, location, gender, age, about);
                                chatRoomsArr.push({
                                    _chatWithUser: user2_id,
                                    _chatNode: currChatNode,
                                    _chatWithUserName: name,
                                    _chatWithUserProfilePic: _profilePic,
                                    _chatWithUserLastMsg: _msgTxt,
                                    _isChatMsgUnread: _msgReadByUser_1,
                                    _chatWithUserLastMsgTime: _lastMsgTime,
                                    _unreadMsgCount: unreadMsgCountUser_1,
                                });
                                console.log('#MessageTab: usersArr at user 2', chatRoomsArr);
                                _this.setState({chatUsersArr: chatRoomsArr});
                            });
                        } catch (error) {
                            console.log('QueryData: error: ', error);
                        }
                    } else {
                        try {
                            usersNodeRef.child(user1_id + '/profile').on('value', function (snapshot) {
                                let profileData = snapshot.val();
                                let name = profileData._userName;
                                let location = profileData._userLocation;
                                let gender = profileData._userSex;
                                let age = profileData._userAge;
                                let about = profileData._userAbout;
                                let _profilePic = profileData._userProfilePic;
                                console.log('#MessagesTab: Data found at _fetchAllChatRooms:', name, location, gender, age, about);
                                chatRoomsArr.push({
                                    _chatWithUser: user1_id,
                                    _chatNode: currChatNode,
                                    _chatWithUserName: name,
                                    _chatWithUserProfilePic: _profilePic,
                                    _chatWithUserLastMsg: _msgTxt,
                                    _isChatMsgUnread: _msgReadByUser_2,
                                    _chatWithUserLastMsgTime: _lastMsgTime,
                                    _unreadMsgCount: unreadMsgCountUser_2,
                                });
                                console.log('#MessageTab: usersArr at user 1', chatRoomsArr);
                                _this.setState({chatUsersArr: chatRoomsArr});
                            });
                        } catch (error) {
                            console.log('QueryData: error: ', error);
                        }
                    }
                }
            });
        });
    }

    showBatchIcon(msgCount) {
        return (
            <View>
                <View style={{
                    height: 20, width: 20, borderRadius: 10, backgroundColor: '#F37678', alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Text style={{fontSize: 12, color: '#FFFFFF'}}>
                        {msgCount}
                    </Text>
                </View>
            </View>
        )
    }

    _onListItemClick(item) {
        let _this = this;
        let singleChatRoomNode = item._chatNode;
        let chatWithUser = item._chatWithUserName;
        let chatRoomsRef = firebase.database().ref(keyNodeChatRooms);
        chatRoomsRef.child(singleChatRoomNode).once('value', function (snapshot) {
            if (snapshot.exists()) {
                printLog('MessagesTab: node ' + singleChatRoomNode + ' already exists.');
                let singleChatRoomData = snapshot.val();
                let roomStatus = singleChatRoomData._room_status;
                let user1Key = roomStatus._user_1_id;
                let currUserKey = HomeTab._getCurrentUserNodeKey();
                printLog('MessagesTab: onListItemClick: ' + user1Key + ' ' + currUserKey + ' ' + chatWithUser);
                if (user1Key === currUserKey) {
                    chatRoomsRef.child(singleChatRoomNode + keySingleChatRoomStatus + '/_isUser_1_online')
                        .set(true).then(() => {
                        printLog('MessagesTab: onListItemClick user 1 status set to online.');
                        chatRoomsRef.child(singleChatRoomNode + keySingleChatRoomStatus + '/_lastMsg/_readByUser_1')
                            .set(true).then(() => {
                            chatRoomsRef.child(singleChatRoomNode + '/_unreadByUser_1').set(0).then(() => {
                                _this._goToOneToOneChatScreen(singleChatRoomNode, chatWithUser);
                            });
                        });
                    });
                } else {
                    chatRoomsRef.child(singleChatRoomNode + keySingleChatRoomStatus + '/_isUser_2_online')
                        .set(true).then(() => {
                        printLog('MessagesTab: onListItemClick user 2 status set to online.');
                        chatRoomsRef.child(singleChatRoomNode + keySingleChatRoomStatus + '/_lastMsg/_readByUser_2')
                            .set(true).then(() => {
                            chatRoomsRef.child(singleChatRoomNode + '/_unreadByUser_2').set(0).then(() => {
                                _this._goToOneToOneChatScreen(singleChatRoomNode, chatWithUser);
                            });
                        });


                    });
                }
            } else {
                printLog('MessagesTab: node ' + singleChatRoomNode + ' does not exists.');
            }
        });
    }

    _goToOneToOneChatScreen(singleChatRoomNode, chatWithUser) {
        this.props.navigator.push({
            screen: 'ChatScreen',
            passProps: {_propsChatNode: singleChatRoomNode, propsChatUserName: chatWithUser}
        });
    }

    showFriendList() {
        let usersArr = this.state.chatUsersArr;
        let view = [];
        console.log('#MessageTab: showFriendsList', usersArr.length);
        for (let i = 0; i < usersArr.length; i++) {
            let pic = usersArr[i]._chatWithUserProfilePic;
            let isUnread = usersArr[i]._isChatMsgUnread;
            let _msgTime = usersArr[i]._chatWithUserLastMsgTime;
            let msgTime = '';
            if (_msgTime !== '') {
                msgTime = moment(usersArr[i]._chatWithUserLastMsgTime).format("hh:m a");
            }
            let count = usersArr[i]._unreadMsgCount;
            view.push(
                <TouchableWithoutFeedback key={i} onPress={() => this._onListItemClick(usersArr[i])}>
                    <View style={styles.friendListItem}>
                        <View style={{flex: 10, flexDirection: 'row'}}>
                            <View style={{flex: 2, alignItems: 'center', justifyContent: 'center'}}>
                                <Image source={(pic && pic !== null) ? {uri: pic} : require('../assets/bg/bg.png')}
                                       style={{height: 50, width: 50, marginHorizontal: 16, borderRadius: 8}}/>
                            </View>
                            <View style={{flex: 6, paddingTop: 8}}>
                                <Text style={{fontSize: 16, marginVertical: 4}}>
                                    {usersArr[i]._chatWithUserName}
                                </Text>
                                {this._showLastMsgTxt(isUnread, usersArr[i]._chatWithUserLastMsg)}
                            </View>
                            <View style={{flex: 2, alignItems: 'center', justifyContent: 'center'}}>
                                <Text style={{fontSize: 12, marginVertical: 4}}>
                                    {usersArr[i]._chatWithUserLastMsg ? msgTime : null}
                                </Text>
                                {count > 0 ? this.showBatchIcon(count) : null}
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            );
        }
        return (view)
    }

    _showLastMsgTxt(val, txtMsg) {
        console.log('MessageTab: ShowLastMagTxt ', val);
        if (!val) {
            return (
                <Text style={{fontWeight: 'bold', fontSize: 16}}>
                    {txtMsg}
                </Text>
            )
        } else {
            return (
                <Text style={{fontSize: 16}}>
                    {txtMsg}
                </Text>
            )
        }
    }

    render() {
        return (
            <View style={{flex: 1, alignItems: 'center', backgroundColor: '#ffffff'}}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{flex: 1, paddingVertical: 8}}>
                    {this.showFriendList()}
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    friendListItem: {
        height: 75,
        width: width * 0.95,
        backgroundColor: '#f6f6f6',
        marginBottom: 16,
        //alignItems:'center',
        justifyContent: 'center',
        borderRadius: 6,
    }
});

export default MessagesTab;