import React, {Component} from 'react';
import {ImageBackground, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View,} from 'react-native';
import * as firebase from "firebase";
import appStyles from "../app_delegates/AppStyles";
import {keyNodeAllUsers, keyNodeProfile} from "./BaseClass";
import HomeTab from "./HomeTab";

class ProfileTab extends Component {

    static navigatorButtons = {
        rightButtons: [
            {
                id: 'edit',
                title: 'Edit',
                buttonColor: '#1E67FF',
            },
        ],
    };

    static navigatorStyle = {
        drawUnderNavBar: false,
        navBarBackgroundColor: '#ffffff',
        topBarElevationShadowEnabled: true,
        statusBarHidden: false,
        navBarComponentAlignment: 'center',
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
        this.state = {
            propsSelectedUserItem: '',
            profile_pic_uri: '',
        }
    }

    onNavigatorEvent(event) {
        if (event.type === 'NavBarButtonPress') {
            if (event.id === 'edit') {
                this._onEditProfileBtnClick();
            }
        }
    }

    componentDidMount() {
        this.props.navigator.setTitle({
            title: 'Profile',
        });
        this._fetchUserProfile();
    }

    _fetchUserProfile() {
        let _this = this;
        let currUserNodeKey = HomeTab._getCurrentUserNodeKey();
        firebase.database().ref(keyNodeAllUsers + currUserNodeKey + keyNodeProfile).on('value', function (snapshot) {
            let data = snapshot.val();
            if (data !== null) {
                let name = data._userName;
                let location = data._userLocation;
                let interestedIn = data._userInterestInMen;
                let age = data._userAge;
                let about = data._userAbout;
                let _profilePic = data._userProfilePic;
                let selectedUserObj = {
                    _name: name,
                    _age: age,
                    _gender: interestedIn ? 'Male' : 'Female',
                    _location: location,
                    _about: about,
                    _key: currUserNodeKey,
                    _userProfilePic: _profilePic
                };
                _this.setState({propsSelectedUserItem: selectedUserObj});
            } else {
                console.log('ProfileTab: at _fetchUserProfile fetched data is null');
            }
        });
    }

    _pushScreenIntoStack(screenName) {
        this.props.navigator.push({
            screen: screenName,
            passProps: {_callFrom: 'ProfileTab'},
        });
    }

    _onEditProfileBtnClick() {
        this._pushScreenIntoStack('ProfileScreen');
    }

    render() {
        let userItem = this.state.propsSelectedUserItem;
        let profile_pic_uri = userItem._userProfilePic;
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={styles.scrollViewContainer}
                keyboardShouldPersistTaps={'handled'}>
                <View style={{flex: 1, backgroundColor: '#f6f5f7'}}>
                    <View style={appStyles.styleBaseViewProfilePic}>
                        <ImageBackground
                            source={(profile_pic_uri && profile_pic_uri != null) ? {uri: profile_pic_uri} : require('../assets/bg/bg.png')}
                            style={{flex: 1}}>

                            <View style={{position: "absolute", bottom: 0, left: 0, marginLeft: 8, marginBottom: 16}}>
                                <Text
                                    ellipsizeMode={'tail'}
                                    numberOfLines={3}
                                    style={styles.baseStyleHeaderText}>
                                    {userItem._name}
                                </Text>
                            </View>

                            <View style={{position: "absolute", bottom: 0, right: 0, marginRight: 4, marginBottom: 8}}>
                                <TouchableWithoutFeedback onPress={() => this._onEditProfileBtnClick()}>
                                    <View style={styles.styleBorderedBtn}>
                                        <Text style={styles.styleTxtMessage}>
                                            Edit Profile
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </ImageBackground>
                    </View>

                    <View style={{flex: 1, flexDirection: 'row',}}>

                        {this._renderProfileDetailView(userItem._age, 'AGE')}
                        <View style={[appStyles.styleVerticalLine, {marginTop: 16}]}/>
                        {this._renderProfileDetailView(userItem._location, 'LOCATION')}
                        <View style={[appStyles.styleVerticalLine, {marginTop: 16}]}/>
                        {this._renderProfileDetailView(userItem._gender, 'INTERESTED')}

                    </View>

                    <View style={{flex: 1, flexDirection: 'column', backgroundColor: '#f6f5f7'}}>
                        <View style={{flex: 1,}}>
                            <Text style={{fontSize: 17, color: '#8C8C99', margin: 24}}>
                                {userItem._about}
                            </Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }

    _renderProfileDetailView(val, holder) {
        return (
            <View style={styles.styleViewProfileDetail}>
                <Text
                    numberOfLines={1}
                    style={styles.styleTxtValue}>
                    {val}
                </Text>
                <Text style={styles.styleTxtPlaceHolder}>
                    {holder}
                </Text>
            </View>
        )
    }

}

const styles = StyleSheet.create({
        scrollViewContainer: {
            flex: 1, backgroundColor: '#f6f5f7'
        },
        baseStyleHeaderText: {
            width: 170,
            fontWeight: 'bold',
            fontSize: 30,
            color: '#ffffff'
        },
        styleTxtMessage: {
            fontWeight: 'bold',
            fontSize: 17,
            color: '#F4C056',
            //fontFamily: 'Cabin-Regular',
        },
        styleBorderedBtn: {
            width: 110,
            height: 36,
            borderColor: '#F8C45A',
            borderRadius: 4,
            borderWidth: 2,
            margin: 12,
            backgroundColor: 'transparent',
            alignItems: 'center',
            justifyContent: 'center',
        },
        styleTxtValue: {
            fontSize: 22,
            color: '#454553',
            //fontFamily: 'Cabin-Regular',
        },
        styleTxtPlaceHolder: {
            fontSize: 10,
            color: '#454553',
            //fontFamily: 'Cabin-Regular',
            opacity: .50,
        },
        styleViewProfileDetail: {flex: 1, flexDirection: 'column', alignItems: 'center', marginTop: 16},
    })
;

export default ProfileTab;