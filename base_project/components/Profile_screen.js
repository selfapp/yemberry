import React, {Component} from 'react';
import {
    Image,
    ImageBackground,
    ScrollView,
    StyleSheet,
    Switch,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View,
} from 'react-native';

import * as firebase from "firebase";
import {Navigation} from "react-native-navigation";
import ImagePicker from 'react-native-image-picker';
import appStyles from "../app_delegates/AppStyles";
import Spinner from 'react-native-loading-spinner-overlay';
import {_startTabBasedAppFromDashboard} from "../index";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import {keyNodeAllUsers, keyNodeProfile, showToastGlobal} from "./BaseClass";

const options = {
    quality: 1.0,
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
        skipBackup: true
    }
};

class ProfileScreen extends Component {

    static navigatorStyle = {
        drawUnderNavBar: false,
        navBarBackgroundColor: '#ffffff',
        topBarElevationShadowEnabled: true,
        statusBarHidden: false,
        navBarComponentAlignment: 'center',
        //collapsingToolBarImage: require('../assets/profile-tab-active/profile-tab-active.png'),
        //collapsingToolBarCollapsedColor: '#0f2362',
    };

    constructor(props) {
        super(props);
        this.props.navigator.setButtons({
            rightButtons: [
                {
                    id: 'done',
                    title: 'Done',
                    buttonColor: '#1E67FF',
                },
            ],
            animated: true,
        });
        let _callType = this.props._callFrom;
        console.log('ProfileScreen: constructor val', _callType);
        if (_callType === 'ProfileTab') {
            this.props.navigator.setButtons({
                leftButtons: [{
                    id: 'cancel',
                    title: 'Cancel',
                    buttonColor: '#1E67FF',
                },
                ],
                animated: true,
            });
        }
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
        this.state = {
            currUserUID: '',
            spinner: false,
            switchMenValue: false,
            switchWomenValue: false,
            profileUserAbout: '',
            profileUserName: '',
            profileUserAge: '',
            profileUserLocation: '',
            profileUserSex: '',
            profile_pic: '',
            profile_pic_uri: '',
        }
    }

    _showSpinner() {
        this.setState({spinner: true});
    }

    _dismissSpinner() {
        this.setState({spinner: false});
    }

    onNavigatorEvent(event) {
        if (event.type === 'NavBarButtonPress') {
            if (event.id === 'done') {
                this._onDoneBtnClick();
            }
            else if (event.id === 'cancel') {
                this._popScreenFromStack('ProfileScreen');
            }
        }
    }

    _popScreenFromStack(screenName) {
        this.props.navigator.pop({
            screen: screenName,
        });
    }

    componentDidMount() {
        this.props.navigator.setTitle({
            title: "Edit Profile"
        });
        let currentUserFromAuth = firebase.auth().currentUser;
        console.log('Profile Screen: node to fetch=>', currentUserFromAuth.uid);
        this._fetchCurrUserProfile(currentUserFromAuth.uid);
    }

    async _fetchCurrUserProfile(currUserUid) {
        this.setState({currUserUID: currUserUid});
        let _this = this;
        await firebase.database().ref(keyNodeAllUsers + currUserUid + keyNodeProfile).on('value', function (snapshot) {
            let data = snapshot.val();
            if (data !== null) {
                let name = data._userName;
                let age = data._userAge;
                let location = data._userLocation;
                let gender = data._userSex;
                let about = data._userAbout;
                let _switchMen = data._userInterestInMen;
                let _switchWomen = data._userInterestInWomen;
                let _profilePic = data._userProfilePic;
                _this._setUserProfileStateVal(name, age, location, gender, about, _switchMen, _switchWomen, _profilePic);
            } else {
                console.log('ProfileScreen: at _fetchUserProfile fetched data is null');
            }
        });
    }

    _setUserProfileStateVal(name, age, location, gender, about, _switchMen, _switchWomen, _profilePic) {
        this.setState({
            switchMenValue: _switchMen,
            switchWomenValue: _switchWomen,
            profileUserAbout: about,
            profileUserName: name,
            profileUserAge: age,
            profileUserLocation: location,
            profileUserSex: gender,
            profile_pic: _profilePic,
        });
    }

    _onDoneBtnClick() {
        let _this = this;
        let switchMen = _this.state.switchMenValue;
        let switchWomen = _this.state.switchWomenValue;
        let userName = _this.state.profileUserName;
        let userAge = _this.state.profileUserAge;
        let userLocation = _this.state.profileUserLocation;
        let userSex = _this.state.profileUserSex;
        let userAbout = _this.state.profileUserAbout;
        let userProfilePic = this.state.profile_pic;
        let _callType = _this.props._callFrom;

        let finalUserGender = userSex.trim().toLocaleLowerCase();

        if (userName.trim().length > 0 && userAge.trim().length > 0 && userLocation.trim().length > 0
            && userSex.trim().length > 0 && (switchMen || switchWomen)) {
            if (finalUserGender === 'male' || finalUserGender === 'female') {
                _this._showSpinner();
                firebase.database().ref(keyNodeAllUsers + _this.state.currUserUID + keyNodeProfile)
                    .set({
                        _userName: userName,
                        _userAge: userAge,
                        _userLocation: userLocation,
                        _userSex: finalUserGender,
                        _userAbout: userAbout,
                        _userInterestInMen: switchMen,
                        _userInterestInWomen: switchWomen,
                        _userProfilePic: userProfilePic,
                    })
                    .then(() => {
                        firebase.database().ref(keyNodeAllUsers + _this.state.currUserUID + '/isProfileComplete')
                            .set(true).then(() => {
                            _this._dismissSpinner();
                            console.log('# ProfileScreen: final call:', _callType);
                            if (_callType === 'ProfileTab') {
                                _this._popScreenFromStack('ProfileScreen');
                                console.log('# ProfileScreen: pop screen here:', _callType);
                            } else {
                                _this._startTabBasedAppFromDashboard();
                                console.log('# ProfileScreen: start tab based app here:', _callType);
                            }
                        });
                    });
            } else {
                _this._dismissSpinner();
                showToastGlobal('Invalid user gender.');
            }
        } else {
            _this._dismissSpinner();
            showToastGlobal('All fields are required.');
        }
    }

    _startTabBasedAppFromDashboard() {
        // Start tab based app for home screen
        console.log('Staring tab based app at profile screen.');
        Navigation.startTabBasedApp({
            tabs: [
                {
                    tabIndex: 0,
                    label: 'Home',
                    screen: 'HomeTab',
                    icon: require('../assets/home-tab-normal/home-tab.png'),
                    selectedIcon: require('../assets/home-tab-active/home-tab-active.png'),
                    title: 'Home'
                },
                {
                    tabIndex: 1,
                    label: 'Messages',
                    screen: 'MessagesTab',
                    icon: require('../assets/message-tab-normal/message-tab.png'),
                    selectedIcon: require('../assets/message-tab-active/message-tab-active.png'),
                    title: 'Messages'
                },
                {
                    tabIndex: 2,
                    label: 'Profile',
                    screen: 'ProfileTab',
                    icon: require('../assets/profile-tab-normal/profile-tab.png'),
                    selectedIcon: require('../assets/profile-tab-active/profile-tab-active.png'),
                    title: 'Profile'
                }
            ],
            tabsStyle: {
                tabBarButtonColor: '#b1b1b1',
                tabBarSelectedButtonColor: '#7a54d3',
                tabBarBackgroundColor: '#FFF',
                initialTabIndex: 0,
            },
            appStyle: {
                tabBarBackgroundColor: 'white',
                tabBarButtonColor: '#b1b1b1',
                tabBarSelectedButtonColor: '#7a54d3',
                tabBarTranslucent: false,
                tabFontFamily: 'Cabin-SemiBold',
                tabFontSize: 16,
                selectedTabFontSize: 16,
                hideBackButtonTitle: true,
            },
            // drawer: {
            //     left: {
            //         screen: 'ProfileTab',
            //         fixedWidth: 700,
            //     },
            // },
        });
    }

    _onEditProfileBtnClick() {
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = {uri: response.uri};
                let sourceBase64 = {uri: 'data:image/jpeg;base64,' + response.data};
                this.setState({profile_pic_uri: response.uri, profile_pic: 'data:image/jpeg;base64,' + response.data});
            }
        });
    }

    _renderProfileImageView() {
        let profile_pic_uri = this.state.profile_pic;
        return (
            <View style={appStyles.styleBaseViewProfilePic}>
                <ImageBackground
                    source={(profile_pic_uri && profile_pic_uri != null) ? {uri: profile_pic_uri} : require('../assets/bg/bg.png')}
                    style={{flex: 1}}>
                    <TouchableWithoutFeedback onPress={() => this._onEditProfileBtnClick()}>
                        <View style={appStyles.styleEditProfileIcon}>
                            <Image source={require('../assets/camera/icon-camera-message.png')}
                            />
                        </View>
                    </TouchableWithoutFeedback>
                </ImageBackground>
            </View>
        )
    }


    render() {
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={styles.scrollViewContainer}
                keyboardShouldPersistTaps={'handled'}>

                <KeyboardAwareScrollView style={{flex: 1}}>

                    <View style={{flex: 1, backgroundColor: '#fff'}}>

                        {this._renderProfileImageView()}

                        <View>
                            <Text style={[styles.baseStyleHeaderText, {marginBottom: 8}]}>
                                Looking for
                            </Text>
                        </View>

                        <View style={[styles.baseContainerLookingFor, {padding: 10}]}>
                            <View style={{flex: 1, alignItems: 'flex-start'}}>
                                <Text style={{fontSize: 18, color: '#707995', marginTop: 4, marginLeft: 6}}>
                                    Men
                                </Text>
                            </View>
                            <View style={{flex: 1, alignItems: 'flex-end'}}>
                                <Switch
                                    onValueChange={(value) => this.setState({
                                        switchMenValue: value,
                                        switchWomenValue: !value
                                    })}
                                    value={this.state.switchMenValue}
                                />
                            </View>
                        </View>

                        <View style={[styles.baseContainerLookingFor, {padding: 10, marginTop: 1,}]}>
                            <View style={{flex: 1, alignItems: 'flex-start'}}>
                                <Text style={{fontSize: 18, color: '#707995', marginTop: 4, marginLeft: 6}}>
                                    Women
                                </Text>
                            </View>
                            <View style={{flex: 1, alignItems: 'flex-end'}}>
                                <Switch
                                    onValueChange={(value) => this.setState({
                                        switchMenValue: !value,
                                        switchWomenValue: value
                                    })}
                                    value={this.state.switchWomenValue}
                                />
                            </View>
                        </View>


                        <View style={styles.baseContainerOptionsView}>
                            <Text style={styles.baseStyleHeaderText}>
                                NAME*
                            </Text>

                            <View style={styles.baseStyleTextInput}>
                                <TextInput
                                    placeholder={'Name'}
                                    maxLength={40}
                                    returnKeyType="done"
                                    placeholderTextColor={'#707995'}
                                    multiline={false}
                                    defaultValue={this.state.profileUserName}
                                    onChangeText={(val) => this.setState({profileUserName: val})}
                                />
                            </View>
                        </View>

                        <View style={styles.baseContainerOptionsView}>
                            <Text style={styles.baseStyleHeaderText}>
                                AGE*
                            </Text>

                            <View style={styles.baseStyleTextInput}>
                                <TextInput
                                    placeholder={'Age'}
                                    maxLength={2}
                                    keyboardType={'numeric'}
                                    placeholderTextColor={'#707995'}
                                    multiline={false}
                                    defaultValue={this.state.profileUserAge}
                                    onChangeText={(val) => this.setState({profileUserAge: val})}
                                />
                            </View>
                        </View>

                        <View style={styles.baseContainerOptionsView}>
                            <Text style={styles.baseStyleHeaderText}>
                                LOCATION*
                            </Text>

                            <View style={styles.baseStyleTextInput}>
                                <TextInput
                                    placeholder={'Location'}
                                    maxLength={40}
                                    returnKeyType="done"
                                    keyboardType={'default'}
                                    placeholderTextColor={'#707995'}
                                    multiline={false}
                                    defaultValue={this.state.profileUserLocation}
                                    onChangeText={(val) => this.setState({profileUserLocation: val})}
                                />
                            </View>
                        </View>

                        <View style={styles.baseContainerOptionsView}>
                            <Text style={styles.baseStyleHeaderText}>
                                SEX*
                            </Text>

                            <View style={styles.baseStyleTextInput}>
                                <TextInput
                                    placeholder={'Sex'}
                                    maxLength={6}
                                    returnKeyType="done"
                                    keyboardType={'default'}
                                    placeholderTextColor={'#707995'}
                                    multiline={false}
                                    defaultValue={this.state.profileUserSex}
                                    onChangeText={(val) => this.setState({profileUserSex: val})}
                                />
                            </View>
                        </View>

                        <View style={[styles.baseContainerOptionsView]}>
                            <Text style={styles.baseStyleHeaderText}>
                                ABOUT
                            </Text>

                            <View style={styles.baseStyleTextInput}>
                                <TextInput
                                    placeholder={'About'}
                                    keyboardType={'default'}
                                    returnKeyType="done"
                                    placeholderTextColor={'#707995'}
                                    multiline={true}
                                    maxLength={150}
                                    defaultValue={this.state.profileUserAbout}
                                    onChangeText={(val) => this.setState({profileUserAbout: val})}
                                />
                            </View>
                        </View>

                        <View style={[styles.baseContainerOptionsView, {marginBottom: 12}]}>
                            <View style={[styles.baseStyleTextInput, {
                                alignItems: 'flex-end',
                                borderBottomColor: '#fff'
                            }]}>
                                <Text style={{marginTop: 0, fontSize: 15, textAlign: 'left', color: '#334068'}}>
                                    {this.state.profileUserAbout.length + '/150'}
                                </Text>
                            </View>
                        </View>

                        <Spinner
                            color={'#54D2E1'}
                            visible={this.state.spinner}
                            cancelable={false}/>

                    </View>

                </KeyboardAwareScrollView>

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: '#fff',
        },
        scrollViewContainer: {
            flex: 1
        },
        baseContainerOptionsView: {
            backgroundColor: '#FFF'
        },
        baseContainerLookingFor: {
            flex: 1,
            flexDirection: 'row',
            height: 44,
            backgroundColor: '#F6F5F7'
        },
        baseStyleHeaderText: {
            marginTop: 22,
            fontWeight: 'bold',
            marginLeft: 16,
            fontSize: 18,
            textAlign: 'left',
            color: '#334068'
        },
        baseStyleTextInput: {
            flex: 1,
            marginLeft: 16,
            marginRight: 16,
            marginTop: 6,
            paddingBottom: 4,
            borderBottomColor: '#e3e7ef',
            borderBottomWidth: 1
        }

    })
;

export default ProfileScreen;
