import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, TextInput, TouchableWithoutFeedback, View,} from 'react-native';
import * as firebase from 'firebase';
import Spinner from 'react-native-loading-spinner-overlay';
import appStyles from "../app_delegates/AppStyles";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import {keyNodeAllUsers, keyNodeAnswers, showToastGlobal} from "./BaseClass";
import _ from 'underscore';

class QuestionnaireScreen extends Component {

    static navigatorStyle = {
        drawUnderNavBar: false,
        navBarBackgroundColor: '#FFF',
        topBarElevationShadowEnabled: true,
        statusBarHidden: false,
        navBarComponentAlignment: 'center',
        statusBarColor: '#000000',
    };

    constructor(props) {
        super(props);
        this.state = {
            spinner: false,
            ques_ans: [],
            modifiedData: [],
            keyboardType: 'default',
            currUserUID: '',
        };
    }

    _pushScreenIntoStack(screenName) {
        this.props.navigator.resetTo({
            screen: screenName,
        });
    }

    _showSpinner() {
        this.setState({spinner: true});
    }

    _dismissSpinner() {
        this.setState({spinner: false});
    }

    componentWillMount() {
        let currentUserFromAuth = firebase.auth().currentUser;
        this._callFirebase(currentUserFromAuth.uid);
    }

    async _callFirebase(uid) {
        let _this = this;
        console.log('Questionnaire Screen: node to fetch=>', uid);
        _this.setState({currUserUID: uid});
        await firebase.database().ref(keyNodeAllUsers + uid).on('value', function (snapshot) {
            console.log('Questionnaire Screen: data fetched is ', snapshot.val());
            let quesArr = snapshot.val();
            if (quesArr != null) {
                _this.setState({ques_ans: quesArr.answers});
                _this.setState({modifiedData: quesArr.answers});
            } else {
                console.log('No user node found at Questionnaire screen.');
            }
        });
    }

    componentDidMount() {
        this.props.navigator.setTitle({
            title: "Questionnaire"
        });
        this.props.navigator.setStyle({
            navBarBackgroundColor: '#FFF',
            navBarComponentAlignment: 'center',
        });
    }

    _saveUserAnswer() {
        let _this = this;
        let arr = this.state.modifiedData;
        for (let i = 0; i < arr.length; i++) {
            let item = arr[i];
            if (item !== null) {
                if (_.isEmpty(item._answer)) {
                    showToastGlobal('Please fill all answers.');
                    return;
                }
            }
        }
        console.log('QuestionnaireScreen: All answers are filled. Now save them and proceed.');
        _this._showSpinner();
        firebase.database().ref(keyNodeAllUsers + this.state.currUserUID + keyNodeAnswers)
            .set(_this.state.modifiedData).then(() => {
            firebase.database().ref(keyNodeAllUsers + this.state.currUserUID + '/isAnswered')
                .set(true).then(() => {
                console.log('QuestionnaireScreen: Answers pushed. Now go to Profile screen.');
                _this._dismissSpinner();
                _this._pushScreenIntoStack('ProfileScreen');
            });
        });
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#D7D7D7'}}>
                <ScrollView
                    keyboardShouldPersistTaps={'handled'}
                    style={styles.scrollViewContainer}
                    showsVerticalScrollIndicator={false}>

                    <KeyboardAwareScrollView>

                        <View style={{flex: 1}}>
                            {this._fetchQuestions()}
                        </View>
                        <View style={appStyles.styleAlignItemsCenter}>
                            <TouchableWithoutFeedback
                                onPress={() => this._saveUserAnswer()}>
                                <View style={appStyles.styleViewBtnBlue}>
                                    <Text style={appStyles.styleGradientBtnTxt}>
                                        SUBMIT
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                        <Spinner
                            color={'#54D2E1'}
                            visible={this.state.spinner}
                            cancelable={false}/>
                    </KeyboardAwareScrollView>
                </ScrollView>
            </View>
        );
    }

    _fetchQuestions() {
        return (
            this.state.ques_ans.map((item, index) => {
                return (
                    this._renderBaseCardView(item, index)
                );
            })
        )
    }

    _renderBaseCardView(item, index) {
        return (
            <View key={index} style={appStyles.styleViewBaseQuestionCard}>
                {this._renderQuestionView(item)}
                {this._renderAnswerView(item, index)}
            </View>
        );
    }

    _renderQuestionView(item) {
        return (
            <View style={styles.styleViewQuestionTxt}>
                <Text style={styles.styleQuestionTxt}>
                    {item._question}
                </Text>
            </View>
        );
    }

    _renderAnswerView(item, index) {
        return (
            <View style={[styles.styleViewQuestionTxt]}>
                <TextInput
                    key={item._id}
                    returnKeyType="done"
                    keyboardType="default"
                    onFocus={() => this.setState({keyboardType: 'default'})}
                    style={[styles.styleAnswerTxt]}
                    placeholder={'Enter your answer here...'}
                    placeholderTextColor={'#D7D7D7'}
                    underlineColorAndroid="transparent"
                    multiline={true}
                    numberOfLines={4}
                    defaultValue={item._answer}
                    onChangeText={(answer) => this._onAnswerTextChange(item, answer, index)}
                />
            </View>
        );
    }

    _onAnswerTextChange(item, answer, index) {
        this.state.modifiedData[index] = {
            _answer: answer.trim(),
            _id: item._id,
            _question: item._question
        }
    }
}

const styles = StyleSheet.create({
    scrollViewContainer: {marginBottom: 2, flex: 1},
    styleQuestionTxtView: {flex: 1},
    styleViewQuestionTxt: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginLeft: 4,
        marginRight: 4,
        marginTop: 4
    },
    styleQuestionTxt: {flex: 1, fontSize: 18, color: '#303030', fontWeight: 'bold'},
    styleAnswerTxt: {flex: 1, fontSize: 18, color: '#303030'},
});

export default QuestionnaireScreen;