import React, {Component} from 'react';
import {
    AsyncStorage,
    Image,
    ImageBackground,
    Keyboard,
    KeyboardAvoidingView,
    ScrollView,
    StatusBar,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View
} from 'react-native';

import * as firebase from "firebase";
import Spinner from 'react-native-loading-spinner-overlay';
import {keyGlobalQuestions, keyIsUserLoggedIn, keyNodeAllUsers, keyUserUID, showToastGlobal} from "./BaseClass";
import LinearGradient from "react-native-linear-gradient";
import appStyles from "../app_delegates/AppStyles";

class SignUpScreen extends Component {

    static navigatorStyle = {
        drawUnderNavBar: true,
        topBarElevationShadowEnabled: false,
        statusBarHidden: true,
        navBarTransparent: true,
        disabledBackGesture: false,
        navBarHidden: false,
        navBarTranslucent: true
    };

    _pushScreenIntoStack(screenName) {
        this.props.navigator.resetTo({
            screen: screenName,
        });
    }

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
        this.state = {
            user_email: '',
            user_password: '',
            user_password_confirm: '',
            spinner: false,
            containerPadding: {padding: 25},
            scrollView: {flex: 1},
        }
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow() {
        this.setState({
            containerPadding: {padding: 0},
            scrollView: {flex: 0}
        });
    }

    _keyboardDidHide() {
        this.setState({
            containerPadding: {padding: 25},
            scrollView: {flex: 1}
        });
    }

    onNavigatorEvent(event) {
        if (event.type === 'NavBarButtonPress') {
            if (event.id === 'back') {
                this._popScreenFromStack('SignUpScreen');
            }
        }
    }

    _popScreenFromStack(screenName) {
        this.props.navigator.pop({
            screen: screenName,
        });
    }

    _funcFireBaseSignUp(email, pass) {
        let _this = this;
        if (_this.state.user_email.length > 0) {
            if (_this.state.user_password.length > 5) {
                if (_this.state.user_password.toString() === _this.state.user_password_confirm.toString()) {
                    try {
                        _this._showSpinner();
                        firebase.auth().createUserWithEmailAndPassword(email, pass)
                            .then(function (user) {
                                console.log('SignUpScreen: uid of newly signed up user:', user.uid);
                                _this._finishSignUpScreenView(user.uid, email);
                            })
                            .catch(function (error) {
                                _this._dismissSpinner();
                                showToastGlobal('Error:\n' + error);
                            });
                    } catch (error) {
                        _this._dismissSpinner();
                        showToastGlobal('Error:\n' + error.toString());
                    }
                } else {
                    _this._dismissSpinner();
                    showToastGlobal('Password and confirm password does not match.')
                }
            } else {
                _this._dismissSpinner();
                showToastGlobal('Password must be greater than or equal to 6 characters')
            }
        } else {
            _this._dismissSpinner();
            showToastGlobal('Email and password fields are blank.')
        }
    }

    async _finishSignUpScreenView(uid, email) {
        let _this = this;
        console.log('SignUpScreen: at finish sign up: uid=>', uid);
        await firebase.database().ref(keyGlobalQuestions).on('value', function (snapshot) {
            let quesArr = snapshot.val();
            if (quesArr != null) {
                console.log('SignUpScreen: Fetched questions array is', quesArr);
                firebase.database().ref(keyNodeAllUsers + uid).set({
                    email: email,
                    isAnswered: false,
                    answers: quesArr,
                }).then(() => {
                    console.log('SignUpScreen: User node ', uid, ' created. Now save userLoggedIn');
                    _this._saveIsUserLoggedIn(uid);
                });
            } else {
                console.log('SignUpScreen: No user node found at Questionnaire screen.');
            }
        });
    }

    _saveIsUserLoggedIn(uid) {
        let _this = this;
        try {
            AsyncStorage.setItem(keyIsUserLoggedIn, 'Yes').then(() => {
                AsyncStorage.setItem(keyUserUID, uid).then(() => {
                    console.log('SignUpScreen: isUserLoggedIn saved. Now go to next screen.');
                    _this._dismissSpinner();
                    _this._pushScreenIntoStack('QuestionnaireScreen');
                    //showToastGlobal('Sign up success.');
                });
            });
        } catch (error) {
            _this._dismissSpinner();
            console.log('SignUpScreen: Error while saving isUserLoggedIn:', error);
        }
    }

    _showSpinner() {
        this.setState({spinner: true});
    }

    _dismissSpinner() {
        this.setState({spinner: false});
    }

    render() {
        return (
            <ImageBackground source={require('../assets/bg/bg.png')} style={{flex: 1}}>
                <View style={appStyles.blackOverlayView}>
                    <ScrollView
                        contentContainerStyle={this.state.scrollView}
                        showsVerticalScrollIndicator={false}
                        keyboardShouldPersistTaps={'handled'}>
                        <KeyboardAvoidingView style={appStyles.styleAvoidKeyboard}>
                            <StatusBar hidden={true}/>
                            <View style={{height: 40}}/>
                            <View style={[appStyles.styleAlignItemsCenter]}>
                                <Image source={require('../assets/logo/logo.png')}/>
                            </View>
                            <View style={{padding: 10}}>
                                <View style={{height: 40}}/>
                                <View style={appStyles.styleViewTxtInputBox}>
                                    <TextInput style={appStyles.styleTxtInputTxt}
                                               placeholder="Enter your email"
                                               maxLength={40}
                                               returnKeyType="done"
                                               keyboardType={'email-address'}
                                               placeholderTextColor={'#D4D4D4'}
                                               underlineColorAndroid="transparent"
                                               value={this.state.user_email}
                                               onChangeText={(email) => this.setState({user_email: email})}
                                    />
                                </View>
                                <View style={appStyles.styleViewTxtInputBox}>
                                    <TextInput style={appStyles.styleTxtInputTxt}
                                               placeholder="Enter password"
                                               maxLength={16}
                                               returnKeyType="done"
                                               keyboardType={'default'}
                                               placeholderTextColor={'#D4D4D4'}
                                               underlineColorAndroid="transparent"
                                               value={this.state.user_password}
                                               secureTextEntry={true}
                                               onChangeText={(password) => this.setState({user_password: password})}
                                    />
                                </View>
                                <View style={appStyles.styleViewTxtInputBox}>
                                    <TextInput style={appStyles.styleTxtInputTxt}
                                               placeholder="Re-enter password"
                                               maxLength={16}
                                               returnKeyType="done"
                                               keyboardType={'default'}
                                               placeholderTextColor={'#D4D4D4'}
                                               underlineColorAndroid="transparent"
                                               value={this.state.user_password_confirm}
                                               secureTextEntry={true}
                                               onChangeText={(password_confirm) => this.setState({user_password_confirm: password_confirm})}
                                    />
                                </View>
                                <View style={appStyles.styleViewGradientBtn}>
                                    <TouchableWithoutFeedback
                                        onPress={() => this._funcFireBaseSignUp(this.state.user_email, this.state.user_password)}>
                                        <LinearGradient
                                            start={{x: 0.0, y: 0.0}} end={{x: 1.0, y: 1.0}}
                                            locations={[0, 0.3, 1.0]}
                                            colors={['#8B5BE6', '#8B5BE6', '#54D2E1']}
                                            style={{flex: 1, padding: 12, borderRadius: 4,}}>
                                            <View>
                                                <Text style={appStyles.styleGradientBtnTxt}>
                                                    SIGN UP
                                                </Text>
                                            </View>
                                        </LinearGradient>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                            <Spinner
                                color={'#54D2E1'}
                                visible={this.state.spinner}
                                cancelable={false}/>
                        </KeyboardAvoidingView>
                    </ScrollView>
                </View>
            </ImageBackground>
        );
    }
}

export default SignUpScreen;
