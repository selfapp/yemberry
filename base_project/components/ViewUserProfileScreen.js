import React, {Component} from 'react';
import {Image, ImageBackground, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View,} from 'react-native';

import * as firebase from "firebase";
import appStyles from "../app_delegates/AppStyles";
import {keyNodeAllUsers, keyNodeProfile, showToastGlobal} from "./BaseClass";

class ViewUserProfileScreen extends Component {

    static navigatorStyle = {
        drawUnderNavBar: false,
        navBarBackgroundColor: '#FFF',
        topBarElevationShadowEnabled: true,
        statusBarHidden: false,
        navBarComponentAlignment: 'center',
        statusBarColor: '#000000',
    };

    constructor(props) {
        super(props);
        this.state = {
            propsSelectedUserItem: '',
        }
    }

    componentDidMount() {
        let _this = this;
        let userItem = this.props.selectedUserItem;
        console.log('AtViewUserProfileScreen', userItem);
        this.props.navigator.setTitle({
            title: userItem._name,
        });
        this._fetchUserProfile(userItem);
    }

    async _fetchUserProfile(userItem) {
        let _this = this;
        await firebase.database().ref(keyNodeAllUsers + userItem._key + keyNodeProfile).on('value', function (snapshot) {
            let data = snapshot.val();
            if (data !== null) {
                let _userPicUri = data._userProfilePic;
                _this.setState({propsSelectedUserItem: userItem});
            } else {
                console.log('ViewUserProfileScreen: at _fetchUserProfile fetched data is null');
            }
        });
    }

    _onMessageBtnClick() {
        showToastGlobal('Coming soon.');
    }

    render() {
        let userItem = this.state.propsSelectedUserItem;
        let profile_pic_uri = userItem._userProfilePic;
        return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={styles.scrollViewContainer}
                keyboardShouldPersistTaps={'handled'}>
                <View style={{flex: 1, backgroundColor: '#f6f5f7'}}>
                    <View style={appStyles.styleBaseViewProfilePic}>
                        <ImageBackground
                            source={(profile_pic_uri && profile_pic_uri != null) ? {uri: profile_pic_uri} : require('../assets/bg/bg.png')}
                            style={{flex: 1}}>
                            <View style={{position: "absolute", bottom: 0, left: 0, marginLeft: 8, marginBottom: 16}}>
                                <Text
                                    ellipsizeMode={'tail'}
                                    numberOfLines={3}
                                    style={styles.baseStyleHeaderText}>
                                    {userItem._name}
                                </Text>
                            </View>

                            <View style={{position: "absolute", bottom: 0, right: 0, marginRight: 4, marginBottom: 8}}>
                                <TouchableWithoutFeedback onPress={() => this._onMessageBtnClick()}>
                                    <View style={[styles.styleBorderedBtn, {flex: 1, flexDirection: 'row'}]}>

                                        <View style={{
                                            alignItems: 'center',
                                            marginTop: 8,
                                            marginBottom: 8,
                                            marginLeft: 8,
                                            justifyContent: 'flex-start'
                                        }}>
                                            <Image
                                                source={require('../assets/message/message-icon.png')}/>
                                        </View>
                                        <View style={{justifyContent: 'center', margin: 8}}>
                                            <Text style={styles.styleTxtMessage}>
                                                Message
                                            </Text>
                                        </View>


                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </ImageBackground>
                    </View>

                    <View style={{flex: 1, flexDirection: 'row',}}>

                        {this._renderProfileDetailView(userItem._age, 'AGE')}
                        <View style={[appStyles.styleVerticalLine, {marginTop: 16}]}/>
                        {this._renderProfileDetailView(userItem._location, 'LOCATION')}
                        <View style={[appStyles.styleVerticalLine, {marginTop: 16}]}/>
                        {this._renderProfileDetailView(userItem._interestedIn, 'INTERESTED')}

                    </View>

                    <View style={{flex: 1, flexDirection: 'column', backgroundColor: '#f6f5f7'}}>
                        <View style={{flex: 1, flexDirection: 'column',}}>
                            <Text style={{fontSize: 17, color: '#8C8C99', margin: 24}}>
                                {userItem._about}
                            </Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }

    _renderProfileDetailView(val, holder) {
        return (
            <View style={styles.styleViewProfileDetail}>
                <Text
                    numberOfLines={1}
                    style={styles.styleTxtValue}>
                    {val}
                </Text>
                <Text style={styles.styleTxtPlaceHolder}>
                    {holder}
                </Text>
            </View>
        )
    }

}

const styles = StyleSheet.create({
        scrollViewContainer: {
            flex: 1, backgroundColor: '#f6f5f7'
        },
        baseStyleHeaderText: {
            width: 170,
            fontWeight: 'bold',
            fontSize: 30,
            color: '#fff'
        },
        styleTxtMessage: {
            fontWeight: 'bold',
            fontSize: 17,
            color: '#F4C056',
            //fontFamily: 'Cabin-Regular',
        },
        styleBorderedBtn: {
            height: 36,
            borderColor: '#F8C45A',
            borderRadius: 4,
            borderWidth: 2,
            margin: 12,
            backgroundColor: 'transparent',
        },
        styleTxtValue: {
            fontSize: 22,
            color: '#454553',
            //fontFamily: 'Cabin-Regular',
        },
        styleTxtPlaceHolder: {
            fontSize: 10,
            color: '#454553',
            //fontFamily: 'Cabin-Regular',
            opacity: .50,
        },
        styleViewProfileDetail: {flex: 1, flexDirection: 'column', alignItems: 'center', marginTop: 16},
    })
;

export default ViewUserProfileScreen;
