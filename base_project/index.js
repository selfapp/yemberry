import {Navigation} from 'react-native-navigation';

import LoginScreen from '../base_project/components/LoginScreen'
import SignUpScreen from '../base_project/components/SignUpScreen'
import ForgotPassword from '../base_project/components/ForgotPassword'
import QuestionnaireScreen from '../base_project/components/QuestionnaireScreen'
import ProfileScreen from './components/Profile_screen'
import HomeTab from './components/HomeTab'
import MessagesTab from './components/MessagesTab'
import ProfileTab from './components/ProfileTab'
import AboutUserScreen from './components/AboutUserScreen'
import MatchScreen from './components/MatchScreen'
import ViewUserProfileScreen from './components/ViewUserProfileScreen'
import ChatScreen from './components/ChatScreen';

import {AsyncStorage,} from 'react-native'
import {keyIsUserLoggedIn, keyNodeAllUsers, keyUserUID} from './components/BaseClass'
import * as firebase from "firebase";
import OneSignal from 'react-native-onesignal';


export default () => {
    Navigation.registerComponent('LoginScreen', () => LoginScreen);
    Navigation.registerComponent('SignUpScreen', () => SignUpScreen);
    Navigation.registerComponent('ForgotPassword', () => ForgotPassword);
    Navigation.registerComponent('QuestionnaireScreen', () => QuestionnaireScreen);
    Navigation.registerComponent('ProfileScreen', () => ProfileScreen);
    Navigation.registerComponent('HomeTab', () => HomeTab);
    Navigation.registerComponent('MessagesTab', () => MessagesTab);
    Navigation.registerComponent('ProfileTab', () => ProfileTab);
    Navigation.registerComponent('AboutUserScreen', () => AboutUserScreen);
    Navigation.registerComponent('MatchScreen', () => MatchScreen);
    Navigation.registerComponent('ViewUserProfileScreen', () => ViewUserProfileScreen);
    Navigation.registerComponent('ChatScreen', () => ChatScreen);


    OneSignal.configure({});

    AsyncStorage.getItem(keyIsUserLoggedIn).then((val) => {
        console.log('index.js isLoggedIn:', val);
        if (val === 'Yes') {
            _getUserEmailAndCheckUserState();
            //_startSingleScreenAppFromLogin('LoginScreen');
        } else {
            // go to LoginScreen
            _startSingleScreenAppFromLogin('LoginScreen');
        }
    });

    function _getUserEmailAndCheckUserState() {
        AsyncStorage.getItem(keyUserUID).then((currUserUid) => {
            console.log('index.js currUserUid:', currUserUid);
            firebase.database().ref(keyNodeAllUsers + currUserUid).once('value').then(function (snapshot) {
                let data = snapshot.val();
                if (data != null) {
                    console.log('index.js snapshot:', data);
                    let isAnswered = data.isAnswered;
                    let isProfileComplete = data.isProfileComplete;
                    console.log('index.js:', isAnswered, isProfileComplete);
                    if (isProfileComplete) {
                        console.log('index.js: go to dashboard.');
                        // go to Dashboard
                        _startTabBasedAppFromDashboard();
                    } else {
                        console.log(isAnswered ? 'index.js: go to profile.' : 'index.js: go to questionScreen.');
                        if (isAnswered) {
                            // go to ProfileScreen
                            _startSingleScreenAppFromLogin('ProfileScreen');
                        } else {
                            // go to QuestionnaireScreen
                            _startSingleScreenAppFromLogin('QuestionnaireScreen');
                        }
                    }
                } else {
                    console.log('index.js: snapshot.val is null');
                }
            });
        });
    }

    function _startTabBasedAppFromDashboard() {
        // Start tab based app for home screen
        Navigation.startTabBasedApp({
            tabs: [
                {
                    tabIndex: 0,
                    label: 'Home',
                    screen: 'HomeTab',
                    icon: require('./assets/home-tab-normal/home-tab.png'),
                    selectedIcon: require('./assets/home-tab-active/home-tab-active.png'),
                    title: 'Home'
                },
                {
                    tabIndex: 1,
                    label: 'Messages',
                    screen: 'MessagesTab',
                    icon: require('./assets/message-tab-normal/message-tab.png'),
                    selectedIcon: require('./assets/message-tab-active/message-tab-active.png'),
                    title: 'Messages'
                },
                {
                    tabIndex: 2,
                    label: 'Profile',
                    screen: 'ProfileTab',
                    icon: require('./assets/profile-tab-normal/profile-tab.png'),
                    selectedIcon: require('./assets/profile-tab-active/profile-tab-active.png'),
                    title: 'Profile'
                }
            ],
            tabsStyle: {
                tabBarButtonColor: '#b1b1b1', // optional, change the color of the tab icons and text (also unselected). On Android, add this to appStyle
                tabBarSelectedButtonColor: '#7a54d3', // optional, change the color of the selected tab icon and text (only selected). On Android, add this to appStyle
                tabBarBackgroundColor: '#FFF', // optional, change the background color of the tab bar
                initialTabIndex: 0,
            },
            appStyle: {
                tabBarBackgroundColor: '#fff',
                tabBarButtonColor: '#b1b1b1',
                tabBarSelectedButtonColor: '#7a54d3',
                tabBarTranslucent: false,
                tabFontFamily: 'Cabin-SemiBold',  // existing font family name or asset file without extension which can be '.ttf' or '.otf' (searched only if '.ttf' asset not found)
                tabFontSize: 16,
                selectedTabFontSize: 16,
                hideBackButtonTitle: true,
            },
            // drawer: { // optional, add this if you want a side menu drawer in your app
            //     left: { // optional, define if you want a drawer from the left
            //         screen: 'ProfileTab', // unique ID registered with Navigation.registerScreen
            //         fixedWidth: 700, // a fixed width you want your left drawer to have (optional)
            //     },
            // },
        });
    }

    function _startSingleScreenAppFromLogin(screen_name) {
        // go to login screen
        Navigation.startSingleScreenApp({
            screen: {
                screen: screen_name,
                navigatorStyle: {},
                navigatorButtons: {}
            },
        });
    }
};